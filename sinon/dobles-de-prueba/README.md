# Lista de contenidos

1. [Uso de *spy* y *stub*](./uso-de-spy-y-stub.md)
2. [Crear *stub* de una dependencia](./stub-de-una-dependencia.md)
