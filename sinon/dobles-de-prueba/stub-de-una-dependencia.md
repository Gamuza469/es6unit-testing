﻿# Crear stub de una dependencia

A la hora de crear nuestros `unit tests`, debemos utilizar mucho las herramientas del tipo `test doubles` que nos proporciona `Sinon.js`. Uno de los casos más comunes es el tener que reemplazar un método importado de una dependencia. Esto podemos solucionarlo de una manera muy sencilla.

Supongamos disponemos de los siguientes archivos:

>**Util.js**

```javascript
module.exports = {
    sumTwoValues: (a, b) => a + b
};
```

>**Account.js**

```javascript
const {sumTwoValues} = require('Util');

class AccountService {
    getAverage (firstAccountBalance, lastAccountBalance) {
        return sumTwoValues(firstAccountBalance, lastAccountBalance) / 2;
    }
}

module.exports = AccountService;
```

En nuestro caso deseamos probar la funcionalidad de **Account.js**. Por ende debemos crear un `stub` de esta clase para probar el resultado del método `getAverage`. El `stub` debe ser declarado antes de importar la clase que va a utilizar este `stub`.

>**Account.spec.js**

```javascript
require('should');
const Util = require('Util');
const utilStub = sinon.stub(Util.prototype, 'sumTwoValues').returns(3000);
const AccountService = require('AccountService');

describe('AccountService', () => {
    describe('#getAverage', () => {
        it('should return the average value', () => {
            const accountService = new AccountService();

            const result = accountService.getAverage(1000, 2000);
            result.should.equals(1500);
            sinon.assert.calledWith(utilStub, 1000, 2000);

            utilStub.restore();

            const result2 = accountService.getAverage(5000, 2000);
            result2.should.equals(3500);
        });
    });
});
```

En nuestra prueba unitaria, podemos ver que antes de importar `AccountService` importamos el módulo `Util` que contiene el método `sumTwoValues` que utiliza internamente el método `getAverage` de la clase `AccountService`. No sólo eso, sino que creamos el `stub` inmediatamente luego de importar `Util`. Esto tiene un porqué y una explicación.

El método `sumTwoValues` se importa a `AccountService` en el momento que este último es importado. Luego de ser importado es imposible crear un `stub` de `sumTwoValues`. Además hay que tener en cuenta: para el caso de necesitar un `stub` de una clase que se debe instanciar, se debe crear el `stub` dentro del `prototype`. Si se trata de un método estático, se debe crear el `stub` dentro de la clase misma.

Aunque es posible realizar `stubs` de clases sin instancias u objetos creados luego de haber sido importada la clase, esto no se aplica a esta situación por una simple razón: **no se pueden crear `stubs` de dependencias dentro de clases una vez que han sido importadas**. Esto se debe al hecho que las dependencias son estáticas al momento de ser importadas y no pueden ser modificadas de ninguna manera.

Al reemplazar (o inyectar) la funcionalidad de las dependencias antes que se importe la clase en cuestión nos permite modificar el funcionamiento interno de los miembros de la misma para un `unit test` conciso. Es decir, que se pueda probar una sola pieza de código, aislada completamente de factores externos. Al finalizar las pruebas, debemos ejecutar `.restore()` para devolver la funcionalidad original a nuestro método `sumTwoValues`.

## Creando stubs de métodos estáticos

Como se menciono más arriba, en el caso de necesitar generar un `stub` para un método estático, se debe crear dentro de la clase misma en vez hacerlo dentro del `prototype`:

>**Util.js (2)**

```javascript
class Util {
    static sumThreeValues (a, b, c) {
        return a + b + c;
    }
}

module.exports = Util;
```

>**Account.js (2)**

```javascript
const {sumThreeValues} = require('Util');

class AccountService {
    getAverage (firstAccountBalance, secondAccountBalance, lastAccountBalance) {
        return sumThreeValues(firstAccountBalance, secondAccountBalance, lastAccountBalance) / 3;
    }
}

module.exports = AccountService;
```

>**Account.spec.js (2)**

```javascript
require('should');
const Util = require('Util');
const utilStub = sinon.stub(Util, 'sumThreeValues').returns(2000);
const AccountService = require('AccountService');

describe('AccountService', () => {
    describe('#getAverage', () => {
        it('should return the average value', () => {
            const accountService = new AccountService();

            const result = accountService.getAverage(1000, 250, 1750);
            result.should.equals(1000);
            sinon.assert.calledWith(utilStub, 1000, 250, 1750);

            utilStub.restore();

            const result2 = accountService.getAverage(3000, 1000, 5000);
            result2.should.equals(3000);
        });
    });
});
```

## Crear stubs: dentro de clase o dentro de objeto

El crear `stubs` dentro de clases funciona y bien. Una vez que uno crea los dobles de prueba puede instanciar cuantos objetos sean necesarios para las pruebas unitarias. Eso si, hay que tener especial cuidado en limpiar los `stubs` implementados por una razón en particular. Si los `stubs` no se limpian al finalizar una suite de pruebas unitarias, los mismos seguirán estando disponibles para toda aquella suite de pruebas unitarias que utilice las clases sobre las cuales hemos sobreescrito sus métodos con dobles de prueba. Como consecuencia se dará a lugar comportamientos erráticos e imprevisibles que pueden verse obfuscados por la cantidad de archivos del proyecto que utilizan pruebas con esa clase. Como no es un error sintáctico, el mismo no saltará a la vista, haciendo su detección mucho más complicada. Es conveniente colocar un `hook` del tipo `after` al final de la ejecución de la suite de pruebas unitarias donde se ejecute el comando `.restore()` para devolver la funcionalidad original.

Sin embargo, no se recomienda el uso de `stubs` dentro de clases sino dentro de instancias de la misma. De esta manera, el problema de `stubs` persistentes no se propagará a otras pruebas unitarias y permitirá que cada suite sea independiente del funcionamiento/ejecución de otras. Incluso se puede obviar el `.restore()` en casos en donde sólo se usarán los `stubs`.

Si bien no se recomienda crear `stubs` dentro de clases, es la única manera de colocarlos en dependencias. Para otros casos donde, por ejemplo, se deban crear dobles de prueba en una clase para reemplazar otros métodos que se usan en la misma clase, es posible crear `stubs` dentro de la misma pero cuando esta se encuentra instanciada:

>**Account.js (3)**

```javascript
class AccountService {
    sumThreeValues (a, b, c) {
        return a + b + c;
    }

    getAverage (firstAccountBalance, secondAccountBalance, lastAccountBalance) {
        return sumThreeValues(firstAccountBalance, secondAccountBalance, lastAccountBalance) / 3;
    }
}

module.exports = AccountService;
```

>**Account.spec.js (3)**

```javascript
require('should');
const AccountService = require('AccountService');

describe('AccountService', () => {
    describe('#getAverage', () => {
        it('should return the average value', () => {
            const accountService = new AccountService();
            const utilStub = sinon.stub(accountService, 'sumThreeValues').returns(2000);

            const result = accountService.getAverage(1000, 250, 1750);
            result.should.equals(1000);
            sinon.assert.calledWith(utilStub, 1000, 250, 1750);

            utilStub.restore();

            const result2 = accountService.getAverage(3000, 1000, 5000);
            result2.should.equals(3000);
        });
    });
});
```

De esta manera podemos obviar el utilizar `utilStub.restore()` para este caso si no probásemos la funcionalidad para `result2`. Podemos evitarlo porque no quedaría referencia del `stub` que sobreviva fuera del contexto de la prueba unitaria por lo que no habrá necesidad de restaurar la funcionalidad original.
