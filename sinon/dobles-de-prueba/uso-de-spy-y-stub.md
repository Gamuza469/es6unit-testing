# Usos de spy y stub

En la documentación de Sinon.js vemos que la diferencia entre un spy y un stub es que el stub permite reemplazar funcionalidad mientra el spy deja que la funcionalidad original se siga ejecutando. Ambos dobles de prueba tomarán contabilidad de las llamadas que se realicen a los métodos los cuales se podrán acceder para determinar el éxito de la prueba unitaria.

Como vimos antes, podremos manipular funcionalidad utilizando sinon.spy() o sinon.stub() sobre clases u objetos. Además, podremos inyectar ambos tipos de dobles de prueba mediante proxyquire. Dependiendo de la forma en que se apliquen los mismos, estos tendrán comportamientos ligeramente diferentes y pueden ser usarse con diferentes finalidades.

## Manipulando funcionalidad usando sinon.spy() y sinon.stub()

En el caso que querramos manipular funcionalidad utilizando los métodos de Sinon.js deberemos descartar utilizar spies. Los spies dejan que se ejecute la funcionalidad original de clases/objetos. En el casos de las pruebas unitarias debemos evitar que se ejecute código que no pertenezca al módulo que estamos probando.

Si manipulamos la funcionalidad usando stubs entonces podremos reemplazar funcionalidad utilizando los métodos conocidos.

Con esta forma, no debemos olvidarnos de restaurar la funcionalidad manipulada al terminar las pruebas sino los dobles de prueba persistirán.

## Inyectando dependencias

Si optamos por utilizar proxyquire para inyectar dependencias nuestros dobles de prueba pueden cumplir fines más específicos a la hora de probar código.

Hay métodos que no son necesarios evaluar el valor de retorno sino que se hayan realizado llamadas a los mismos. Son estos casos donde podemos inyectar un spy en vez de un stub. Es posible colocar un stub también ya que un stub es un spy pero amerita utilizar funcionalidad que no será utilizada.

>**./Log.js**

```javascript
module.export = {
    logActivity: /* method which logs activity to database */
};
```

>**./Checkout.js**

```javascript
const {logActivity} = require('Log');

class Checkout {
    checkSupplies (supplies) {
        logActivity();
        return name && quantity >= 50;
    }
}

module.export = Checkout;
```

>**./Checkout.spec.js**

```javascript
const logActivitySpy = sinon.spy();

const Checkout = proxyquire('Checkout', {
    'Log': {
        logActivity: logActivitySpy
    }
});

const checkout = new Checkout();

describe('Checkout', () => {
    describe('#checkSupply', () => {
        afterEach(() => logActivitySpy.resetHistory());

        context('when the supply has a name', () => {
            context('and its quantity is greater or equal than fifty', () => {
                it('should approve the supply', () => {
                    const supply = {
                        name: 'Apples',
                        quantity: 50
                    };
                    const supplyApproval = checkout.checkSupply(supply);

                    supplyApproval.should.equal.true();
                    logActivitySpy.should.have.been.calledOnce();
                });
            });

            context('and its quantity is less than fifty', () => {
                it('should reject the supply', () => {
                    const supply = {
                        name: 'Apples',
                        quantity: 49
                    };
                    const supplyApproval = checkout.checkSupply(supply);

                    supplyApproval.should.equal.false();
                    logActivitySpy.should.have.been.calledOnce();
                });
            });
        });

        context('when the supply has no name', () => {
            context('and its quantity is greater or equal than fifty', () => {
                it('should reject the supply', () => {
                    const supply = {
                        quantity: 50
                    };
                    const supplyApproval = checkout.checkSupply(supply);

                    supplyApproval.should.equal.false();
                    logActivitySpy.should.have.been.calledOnce();
                });
            });

            context('and its quantity is less than fifty', () => {
                it('should reject the supply', () => {
                    const supply = {
                        quantity: 49
                    };
                    const supplyApproval = checkout.checkSupply(supply);

                    supplyApproval.should.equal.false();
                    logActivitySpy.should.have.been.calledOnce();
                });
            });
        });
    });
});
```

Para inyectar dependencias y reemplazar funcionalidad la cual vamos a evaluar su valor de retorno simplemente utilizamos un stub que podremos indicarle exactamente que debe retornar.

Otra ventaja de esta forma de manipular funcionalidad es que no es necesario restaurar funcionalidades originales salvo aquellos dobles de prueba que son utilizados en más de una prueba unitaria.
