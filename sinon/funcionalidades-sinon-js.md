﻿# Descripción de funcionalidades de Sinon.js

Para entender las diferentes funcionalidades de Sinon.js se recomienda ver la siguiente lista de reproducción de YouTube. Consisten en cuatro videos, totalizando 50 minutos de aprendizaje. Los mismos abarcan `spies`, `stubs`, `mocks`, `fakes` y sobre cómo utilizar Sinon.js para realizar `unit testing`.

Pueden encontrar la lista de reproducción [aquí](https://www.youtube.com/watch?v=SvudHPTEsIk&list=PLXRl2mzlfQJ0v13jIcGuwwJWHtqMmO8R0).

## Spies

Es un método que toma nota de los argumentos con los que se llamó el método original, el valor de retorno, el valor del objeto `this` dentro del método, excepciones y las llamadas realizadas al método.

Se puede espiar un método, un método perteneciente a un objeto o utilizarse como `callback`. En todo caso, se ejecuta el método original.

## Stubs

Es un `spy` con código preprogramado. Permite reemplazar el código original de un método. Se utiliza en los casos que necesitamos que un método tenga una funcionalidad fija de tal manera que el valor retornado al llamar al método es siempre el mismo. Dicho de otra manera, con un `stub` podemos forzar el código para que funcione de una manera específica con el fin de probar diferentes flujos de ejecución del código.

El fuerte de esta funcionalidad es la de reemplazar las llamadas a una API y otros tipos de métodos asincrónicos.

Se puede reemplazar un método perteneciente a un objeto, todos los métodos de un objeto o utilizarse como `callback`. En todo caso, se ejecuta el método original.

No se debe olvidar de restaurar la funcionalidad original del método utilizando `.restore()`.

## Mocks o fakes

Son métodos similares a los `spies` con código preprogramado, como un `stub`, del cuál se genera una expectativa o `expectation` de que se utilize de una manera en particular.

Por regla general, se debe usar solamente un `mock` por `test`. La razón es que los `mocks` vuelven al `test` muy específico y muy sensible a cambios en el código original del método sobre el cuál se generó el `mock`.

Al final de la ejecución se debe verificar si se cumplió con la expectativa utilizando `.verify()`.

## Fake timers

Implementación sincrónica de los métodos `setTimeout`, `clearTimeout`, `setInterval`, `clearInterval` y `Date`. Permiten manipular código que tenga funcionalidad relacionada con un momento.

Esto permite simular el paso del tiempo de manera controlada y así probar código asincrónico relacionado con eventos temporales.

No se debe olvidar de restaurar la funcionalidad original del reloj interno utilizando `.restore()`.

## Fake XHR & server

Implementación que permite simular de manera específica la respuesta a una llamada AJAX. Se crea un servidor simulado donde se genera una respuesta fija  para un `HTTP method` y `URL` específicos en cuanto a `HTTP status code`, `headers` y `response body`.

No se debe olvidar de restaurar la funcionalidad original del servidor simulado utilizando `.restore()`.
