# Lista de contenidos

1. [Funcionalidades de Sinon.js](./funcionalidades-sinon-js.md)
2. [Dobles de prueba](./dobles-de-prueba/README.md)
3. [Extendiendo funcionalidades de Sinon.js](./extendiendo-funcionalidad-sinon.md)
