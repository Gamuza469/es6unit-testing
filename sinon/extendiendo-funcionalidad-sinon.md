# Extendiendo las funcionalidades de Sinon.js

## Utilizando *alias* para referirse a un `assertion`

Esto es útil cuando queremos modificar la manera en la que escribimos el `assertion`, sea por la semántica que usamos o porque el nombre del método no se conjuga bien con el objeto `should` o no representa al tipo de verificación que realiza.

En nuestro caso, la biblioteca **should-sinon** cuenta con **Assertion#threw** para verificar si un `spy` o `stub` lanzó una excepción. La biblioteca **should-sinon** se creó para poder utilizar `sinon.assert` de una manera semántica, utilizando la biblioteca `should`. No obstante, a la hora de escribir nuestro `assertion` nos damos cuenta de la realidad:

```javascript
it('should check the exception', () => {
    const executeCode = sinon.stub().throws();

    try {
        executeCode();
    } catch (error) {
        executeCode.should.threw();
    }
});
```

La forma del verbo `threw` no se corresponde con `should`. Por ende necesitamos cambiar la manera en que se llama al `assertion`.

Utilizando el método `alias` del objeto `assertion` podemos asignar un nuevo nombre el cual, al ser invocado, estará llamando al `assertion` original. Este nuevo nombre no evita que se pueda llamar al `assertion` de la forma original.

Hay dos maneras de crear nuestro `alias`. Esto dependerá si queremos tomar una aproximación estilo **plugin** o estilo **declarativo**. Si no se esta escribiendo un nuevo `assertion` o no forma parte de un grupo de `aliases`, se recomienda utilizar la forma corta.

>**Nota**
Si se utiliza mediante `should.use`, podemos importar la implementación a partir de otro módulo.

```javascript
const should = require('should');

// The plugin way
should.use(function(should, Assertion) {
    Assertion.alias('threw', 'thrown');
});

// The short way
should.Assertion.alias('threw', 'thrown');
```

Con esto, podremos escribir nuestro `assertion` de una forma más agradable y fácil de leer:

```javascript
it('should check the exception', () => {
    const executeCode = sinon.stub().throws();

    try {
        executeCode();
    } catch (error) {
        executeCode.should.have.thrown();
    }
});
```
