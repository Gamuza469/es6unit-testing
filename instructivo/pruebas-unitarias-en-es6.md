# Implementar pruebas unitarias en proyecto ES6

Para ayudar a la comprensión de los temas y ejemplos tratados en este instructivo se recomienda descargar el siguiente repositorio. El mismo consta de un proyecto con todos los paquetes y configuración necesaria para correr pruebas unitarias de manera sencilla y rápida.

```http
http://gitlab.com/Gamuza469/mocha-es6
```

Este proyecto necesita la instalación de paquetes `npm`. Para ello es necesario instalar `npm`:

```sh
sudo apt-get install npm
```

Luego debemos instalar todos los paquetes necesarios para correr el proyecto:

```sh
npm install
```

Para correr las pruebas del proyecto debemos tipear:

```sh
npm test
```

Luego de ejecutar las pruebas unitarias se mostrarán los resultados de las mismas.

## Prólogo

Este instructivo tiene como objetivo principal el de guiar y enseñar al lector sobre el concepto de las pruebas unitarias o `unit tests`. El documento repasa conceptos básicos acerca de los elementos pertinentes a la prueba de módulos de código, los pasos a seguir y las herramientas necesarias para implementarlas tanto en nuevos proyectos como proyectos existentes.

Sin embargo, en la práctica la implementación suele ser muy distinta entre proyectos. Teniendo en cuenta el lenguaje utilizado, entornos de trabajo (frameworks), metodologías de desarrollo utilizadas, bibliotecas, entorno de pruebas, etc. Esto resulta en una implementación de pruebas unitarias que puede diferir de lo explicado en este instructivo, tanto en las herramientas utilizadas como en los conceptos.

Por ello durante la redacción del texto y explicación de los temas se optó por tomar una aproximación lo más aislada del lenguaje posible. Esto considerando que la implementación se realizará en un entorno que utilice ECMAScript 6.

Este instructivo es un WIP. Si bien los temas principales estan abordados se incluirán más temas y más ejemplos a medida que sea posible.

## Tabla de contenidos

1. Pasos iniciales
   a. Estructura de archivos
   b. Paquetes necesarios (WIP)
   c. Configuración del entorno
2. Trabajando con pruebas unitarias
   a. Creando una prueba unitaria
   b. Evaluando los resultados de las pruebas unitarias
3. Assertions
   a. Sintaxis BDD
   b. Evaluando propiedades
   c. Concatenando assertions
   d. Evaluando flujos de ejecución
   e. Buenas prácticas
4. Preparación del código
   a. Cómo aislar el módulo para pruebas
   b. Uso de dobles de prueba
   c. Inyección de dependencias
5. Assertions en elementos complejos
   a. Evaluando componentes React

## 1. Configuración

### a. Estructura de archivos

Para separar nuestros archivos de código de nuestros archivos de pruebas unitarias debemos establecer una estructura que los segregue correctamente:

```text
project
|---src
|   |---services
|       |---util.js
|
|---test
|   |---src
|   |   |---services
|   |       |---util.spec.js
|   |
|   |---mocha.opts
|   |---setup.js
|
|---index.js
|---package.json
|---package-lock.json
```

Por un lado tenemos la carpeta `src` donde concentramos la lógica del proyecto y tenemos una carpeta `test` en donde colocaremos nuestros archivos de pruebas unitarias. La carpeta `test` incluye en su raíz los archivos `mocha.opts` (que indica a **Mocha** qué archivos cargar) y `setup.js` (que configura e importa los módulos necesarios al entorno de pruebas unitarias). La subcarpeta `src` contendrá una estructura similar (sino idéntica) a la carpeta `src`, que se encuentra en la raíz del proyecto, y cada subcarpeta contendrá archivos de pruebas unitarias nombrados de manera similar. La diferencia radica en que los nombres de archivos tendrán anexada la palabra `.spec` antes de la extensión `.js`. Los únicos archivos a probar serán los archivos `.js`.

### b. Paquetes necesarios

(WIP)

### c. Configuración del entorno

Necesitaremos configurar los archivos `mocha.opts` y `setup.js` antes de ejecutar nuestras pruebas unitarias.

#### mocha.opts

El propósito de este archivo es el de recibir los argumentos que típicamente se le pasan a **Mocha** a través de la línea de comando. La razón de escribir este archivo es porque al ejecutar **Mocha**, éste automáticamente busca el archivo `mocha.opts` en la carpeta `test` y lee cada línea como un argumento. De esta manera sólo tenemos que llamar a **Mocha** sin argumentos que la aplicación buscará los mismos de manera automática.

Para empezar, en un nuevo proyecto no necesitaremos demasiada configuración:

```sh
--require ./test/setup
./test/**/*.spec.js
```

En este archivo indicamos que el archivo `./test/setup.js` sea importado mediante `require()` de **NodeJS**. Se hace `require` de archivos de configuración de paquetes del proyecto entre otras cosas.

Además indicamos cuáles son los archivos de pruebas unitarias con los que debe trabajar. La ruta especificada debe estar escrita como un **glob pattern**.

Luego, debemos configurar nuestro entorno de pruebas.

#### setup.js

Esta es la configuración mínima necesaria para que nuestro proyecto realice las pruebas unitarias. Este archivo debe escribirse de manera que **NodeJS** y **Mocha** lo puedan interpretar (es decir, utilizando `require()` y `module.exports`).

```javascript
const should = require('should');
const sinon = require('sinon');
require('should-sinon');
require('@babel/register')();

process.env.NODE_ENV = 'test';

module.exports = {
    should,
    sinon
};
```

Aquí inicializamos las bibliotecas `should` y `sinon`, además de `should-sinon` que agrega funcionalidad a `should`.

Se configura también la variable de entorno `NODE_ENV` de modo tal que nos facilite configurar cualquier otra biblioteca o elemento que debamos agregar y se maneje con entornos. Esto también nos servirá si debemos establecer una configuración particular para la transpilación con **Babel**.

Para poder utilizar ES6 en nuestro proyecto debemos ejecutar `@babel/register` como lo vemos aquí. Atención porque no sólo se importa el módulo sino que se ejecuta también.

## 2. Trabajando con pruebas unitarias

### a. Creando una prueba unitaria

Al crear una prueba unitaria debemos respetar la estructura declarativa de las mismas, que detallan con precisión lo que se esta probando, la funcionalidad que se quiere probar y las circunstancias en las que se ejecuta la funcionalidad.

#### Estructura de una prueba unitaria

```javascript
declare('UtilService', () => {
    declare('.sum', () => {
        context('when there are three numbers', () => {
            it('should sum all numbers', () => {
                // unit-test assertions
            });
        });
    });
});
```

La estructura consta de funciones que se llaman con dos parámetros: una cadena descriptiva y una función tipo `callback`. Estas funciones (o bloques) se declaran de manera organizada en función del código que se prueba. Existen tres tipos de bloques:

> **declare**

Se crea un bloque `declare` para explicitar sobre qué elemento se realizan las pruebas unitarias. En nuestro ejemplo principal existen dos bloques `declare`. Nuestro primero bloque detalla el elemento dentro del cuál se hallará la función que se irá a probar. El segundo bloque detalla el nombre de la función que se probará. Dentro de este segundo bloque `declare` se encontrarán nuestras pruebas unitarias.

El primer bloque `declare` constará del nombre de la clase y aquellos prefijos/sufijos que sean necesarios para distinguirlos de elementos similares. Como vemos en nuestro ejemplo, nombramos al bloque como `UtilService` porque estaremos probando las funciones de nuestro servicio `util.js` ubicado en la carpeta `services`.

El segundo bloque `declare` contendrá el nombre de la función o método que queremos probar. Para el ejemplo, se escribe `.sum` dado que el método tiene el nombre `sum` y este va precedido de un punto para identificar el método como un método de clase. Se pueden identificar los métodos según su contexto de invocación:

* Si el método es estático, es un método de clase y por ende se prefija con punto: `.`.
* Si el método debe provenir de una instancia de clase es un método instanciado y se prefija con numeral: `#`.

Dentro del bloque `declare` que define sobre qué elemento se harán las pruebas unitarias vendrán los bloques de las pruebas unitarias propiamente dichas. Aquí dentro podremos encontrar bloques `context` y  más importante los bloques `it`.

> **it**

Dentro de estos bloques se encontrará la lógica de las pruebas unitarias. Generalmente se prueba una funcionalidad en particular por bloque, es decir, un bloque `it` por funcionalidad probada. El bloque `it` tendrá incluída una descripción de la funcionalidad que debe llevar a cabo el código que estamos probando, la cual se probará en base a las aseveraciones o `assertions` que declararemos dentro del bloque.

La manera correcta de escribir la descripción es escribir lo que se espera que la función o pieza de código haga o cumpla. Es decir, lo que necesitamos que funcione de una manera determinada para asegurarnos que la función hace lo que se espera de la misma y no tenga comportamientos no controlados, no esperados o erróneos al ser ejecutados en nuestro sistema. En el ejemplo vemos que describe la prueba unitaria como `it should sum all numbers`. Si bien el `it` es el nombre del bloque, se lee junto con la descripción. La descripción además debe empezar con el verbo `should` para indicar qué se supone que debe hacer el código. Podemos ver que se espera que el método `sum` sume todos los números. Se puede decir con esto que la descripción esta, en cierta forma, ligada al resultado al que debe llegar la ejecución del código.

> **context**

Para probar ciertas funcionalidades hay que considerar probar diferentes casos en los que se pueda utilizar las mismas. Esto es, si el resultado al ejecutar la funcionalidad cambia según su contexto entonces debemos abarcar esos contextos.

Para poder entender el concepto del uso de los bloques `context` tomemos el siguiente ejemplo: supongamos una función que evalúa la edad. Esta función evalúa si la persona es mayor de edad; si lo es entonces devuelve `true`, de lo contrario devolverá `false`. Sin embargo, si la persona es menor emancipado (mayor de 16 años que vive de manera independiente con consentimiento de los padres) entonces también devolverá `true`. Esto nos llevará a la siguiente tabla de verdad:

|Edad|¿Es menor emancipado?|Valor de verdad|
|---|---|---|
|Menor a 18|No|*false*|
|Menor a 18|Si|**true**|
|Mayor o igual a 18|No|**true**|
|Mayor o igual a 18|Si|**true**|

Vemos que para un menor no emancipado, la funcionalidad debería devolver `false`. Llevando esto a una condición nos quedaría una función:

```javascript
export default {
    verifyPersonIsAdult: ({age, isEmancipated}) => return age >= 18 || isEmancipated;
};
```

Entonces de la condición podremos deducir tres casos importantes, cuando es menor de edad y no esta emancipado, cuando es menor de edad pero esta emancipado o cuando es mayor de edad.

```javascript
context('when the person has or is older than 18 years old', () => {
    // unit-test code
});

context('when the person is less than 18 years old and it is not emancipated', () => {
    // unit-test code
});

context('when the person is less than 18 years old and it is emancipated', () => {
    // unit-test code
});
```

Vemos que los contextos empiezan con la palabra `when` seguido del objeto que será argumento de la función junto con el estado del objeto. Todo esto definirá el contexto de nuestra prueba. Además, en el caso que una parte del contexto sea compartida con otra prueba podremos escribir el contexto de la siguiente manera:

```javascript
context('when the person has or is older than 18 years old', () => {
    // unit-test code
});

context('when the person is less than 18 years old', () => {
    context('and it is not emancipated', () => {
        // unit-test code
    });

    context('and it is emancipated', () => {
        // unit-test code
    });
});

```

Completando nuestro ejemplo, agregamos los bloques restantes:

```javascript
describe('AgeService', () => {
    describe('.verifyPersonIsAdult', () => {
        context('when the person has or is older than 18 years old', () => {
            it('should verify correctly', () => {
                // unit-test code
            });
        });

        context('when the person is less than 18 years old', () => {
            context('and it is not emancipated', () => {
                it('should not verify', () => {
                    // unit-test code
                });
            });

            context('and it is emancipated', () => {
                it('should verify correctly', () => {
                    // unit-test code
                });
            });
        });
    });
});
```

#### Escribiendo las aseveraciones (assertions)

Una vez definidos los contextos debemos explicitar cómo debe darse el resultado o retorno de la función que estamos probando. Para ello debemos definir cómo o con qué características estará definido el valor de retorno. Esto se logra definiendo aseveraciones o `assertions`. La sintaxis se verá más adelante pero es suficientemente clara como para entenderla sin necesidad de saber como se escriben las mismas.

```javascript
module.exports = {
    verifyPersonIsAdult: ({age, isEmancipated}) => return age >= 18 || isEmancipated;
};
```

```javascript
describe('AgeService', () => {
    describe('.verifyPersonIsAdult', () => {
        context('when the person has or is older than 18 years old', () => {
            it('should verify correctly', () => {
                const person = {
                    age: 18,
                    isEmancipated: false
                };

                const isAdult = AgeService.verifyPersonIsAdult(person);

                isAdult.should.be.true();
            });
        });
    });
});
```

Al escribir los assertions, estos deben definir el resultado de la ejecución dependiendo del contexto. Más precisamente, debemos definir puntualmente lo que se espera que la funcionalidad retorne luego de ser ejecutada. En el primer caso del ejemplo anterior, esperamos que la función nos retorne `true` para el caso que la persona tenga 18 o más años de edad.

En el ejemplo actual, podemos apreciar tres sentencias dentro de nuestra prueba unitaria. Podemos distinguir un objeto `person` que creamos arbitrariamente, el cual contiene los atributos `age` y `isEmancipated`. Los valores de estos atributos están definidos arbitrariamente tomando como referencia la descripción de nuestros bloques `context`. El objeto `person` entonces será enviado como argumento al método `verifyPersonIsAdult` para ser procesado. El método devolverá un valor de resultado el cual guardamos en la variable `isAdult`. Según nuestra prueba unitaria esperamos que el valor de retorno sea un *boolean* con valor `true` que es lo que evaluamos en la última sentencia.

Probemos con las demás pruebas unitarias:

```javascript
describe('AgeService', () => {
    describe('.verifyPersonIsAdult', () => {
        context('when the person has or is older than 18 years old', () => {
            it('should verify correctly', () => {
                const person = {
                    age: 18,
                    isEmancipated: false
                };

                const isAdult = AgeService.verifyPersonIsAdult(person);

                isAdult.should.be.true();
            });
        });

        context('when the person is less than 18 years old', () => {
            context('and it is not emancipated', () => {
                it('should not verify', () => {
                    const person = {
                        age: 16,
                        isEmancipated: false
                    };

                    const isAdult = AgeService.verifyPersonIsAdult(person);

                    isAdult.should.be.false();
                });
            });

            context('and it is emancipated', () => {
                it('should verify correctly', () => {
                    const person = {
                        age: 16,
                        isEmancipated: true
                    };

                    const isAdult = AgeService.verifyPersonIsAdult(person);

                    isAdult.should.be.true();
                });
            });
        });
    });
});
```

### b. Evaluando los resultados de las pruebas unitarias

(WIP)

## 3. Assertions

### a. Sintaxis BDD

En nuestras pruebas unitarias se utiliza la sintaxis que se emplea en Behavior-Driven Development o BDD. En este tipo de desarrollo se definen los comportamientos que debe tener una aplicación según su contexto y el evento que sucede en un momento. La sintaxis empleada en BDD al definir los comportamientos consta principalmente de oraciones en inglés que explican el contexto en el que se ejecuta una funcionalidad y qué es lo que debe suceder finalmente.

Para nuestra implementación, seguiremos algunos de los pasos que se usan en la sintaxis BDD:

* Al definir un bloque de contexto `context`, este debe empezar con la palabra `when`.
  * De necesitar ser más específico, se debe agregar otro bloque `context` que debe empezar con la palabra `and`. Esto puede darse en casos que haya más de una prueba unitaria que tengan una parte del contexto en común o en el caso que haya un contexto complejo.
  * Es necesario colocar al sujeto de manera natural, no se debe emplear términos de programación en lo posible (`'person'` vs `Person` o `Person object`).
  * Se debe incluir un atributo necesario para el contexto en el predicado con el verbo `is`.
* Para definir el bloque de prueba unitaria `it`, este debe empezar con la palabra `should`.
* Las descripciones deben estar escritas en `simple present tense`.
* Los `assertion` deben escribirse empezando por el sujeto seguido de la palabra `should` y el resto de la oración en `simple present`.

### b. Evaluando propiedades

Puede que en una prueba unitaria estemos en la necesidad de corroborar las propiedades de un objeto que es resultado de una función que estamos probando. Para ello utilizamos los métodos incluídos en la biblioteca `should` que además nos permiten formular `assertions` en lenguaje inglés de forma natural.

> En esta sección se verán unos pocos métodos de evaluación. Para más métodos se puede acceder a la sección **assertions** de la documentación.

Modificaremos un poco nuestro método `verifyPersonIsAdult`:

```javascript
export default {
    verifyPersonIsAdult: ({age, isEmancipated}) => {
        const result = {
            isAdult: false,
            isMinor: true
        };

        if (age >= 18) {
            result.isAdult = true;
            result.isMinor = false;
        }

        if (isEmancipated) {
            result.isAdult = true;

            if (age < 16) {
                result.exceptionalEmancipation = true;
            }
        }

        return result;
    }
};
```

Se ha modificado el método con las consigna que devuelva un objeto que tenga dos propiedades: `isAdult`
y `isMinor` con diferentes valores de verdad y una tercera propiedad que saldrá sólo si el menor emancipado es menor de 16 años que es `exceptionalEmancipation`. Deberemos modificar nuestras pruebas unitarias para reflejar estos cambios en el código:

```javascript
describe('AgeService', () => {
    describe('.verifyPersonIsAdult', () => {
        context('when the person has or is older than 18 years old', () => {
            it('should verify correctly', () => {
                const person = {
                    age: 18,
                    isEmancipated: false
                };

                const result = AgeService.verifyPersonIsAdult(person);

                result.should.have.property('isAdult');
                result.should.have.property('isMinor');
            });
        });

        context('when the person is between 16 and 18 years old', () => {
            context('and it is not emancipated', () => {
                it('should not verify', () => {
                    const person = {
                        age: 16,
                        isEmancipated: false
                    };

                    const result = AgeService.verifyPersonIsAdult(person);

                    result.should.have.property('isAdult');
                    result.should.have.property('isMinor');
                });
            });

            context('and it is emancipated', () => {
                it('should verify correctly', () => {
                    const person = {
                        age: 16,
                        isEmancipated: true
                    };

                    const result = AgeService.verifyPersonIsAdult(person);

                    result.should.have.property('isAdult');
                    result.should.have.property('isMinor');
                });
            });
        });

        context('when the person is emancipated', () => {
            context('and it is less than 16 years old', () => {
                it('should verify correctly', () => {
                    const person = {
                        age: 14,
                        isEmancipated: true
                    };

                    const result = AgeService.verifyPersonIsAdult(person);

                    result.should.have.property('isAdult');
                    result.should.have.property('isMinor');
                    result.should.have.property('exceptionalEmancipation');
                });
            });
        });
    });
});
```

### c. Concatenando assertions

En el ejemplo anterior escribimos `assertions` para corroborar propiedades y sus valores. Estas sentencias no son más que `assertions` concatenados gracias al poder de la biblioteca `should`. Se combina el `assertion` para ver que el objeto `result` tiene la propiedad `isAdult`. Seguido, se le concatena el `assertion` que se fija que el valor de esa propiedad `isAdult` tiene como valor el booleano `true`.

En la manera que funcionan las concatenaciones, si uno de los `assertion` de la cadena falla entonces fallará toda la cadena pudiendo arrastrar la falla del primer `assertion` a los otros encadenados.

La concatenación de `assertions` puede darse virtualmente sin límite alguno de modo que podemos escribir una sentencia de la siguiente manera:

```javascript
context('when the person is emancipated', () => {
    context('and it is less than 16 years old', () => {
        it('should verify correctly', () => {
            const person = {
                age: 14,
                isEmancipated: true
            };

            const result = AgeService.verifyPersonIsAdult(person);

            result.should.have.property('isAdult').which.equals(true);
            result.should.have.property('isMinor').which.equals(true);
            result.should.have.property('exceptionalEmancipation').which.equals(true);
        });
    });
});
```

Aquí se redefinió la primer sentencia del ejemplo anterior, agregandole a la misma un `assertion` que verifica el valor de cada propiedad. Entonces tenemos dos `assertions` en una misma sentencia. Si así lo deseamos podemos agregar un tercer `assertion` que verifique que `result` sea un objeto:

```javascript
context('when the person is emancipated', () => {
    context('and it is less than 16 years old', () => {
        it('should verify correctly', () => {
            const person = {
                age: 14,
                isEmancipated: true
            };

            const result = AgeService.verifyPersonIsAdult(person);

            result.should.be.an.object().and.have.property('isAdult').which.equals(true);
            result.should.be.an.object().and.have.property('isMinor').which.equals(true);
            result.should.be.an.object().and.have.property('exceptionalEmancipation').which.equals(true);
        });
    });
});
```

Aunque no es buena práctica verificar que sea objeto en cada sentencia sino que es mejor hacerlo una sóla vez:

```javascript
context('when the person is emancipated', () => {
    context('and it is less than 16 years old', () => {
        it('should verify correctly', () => {
            const person = {
                age: 14,
                isEmancipated: true
            };

            const result = AgeService.verifyPersonIsAdult(person);

            result.should.be.an.object();
            result.should.have.property('isAdult').which.equals(true);
            result.should.have.property('isMinor').which.equals(true);
            result.should.have.property('exceptionalEmancipation').which.equals(true);
        });
    });
});
```

### d. Evaluando flujos de ejecución

Para códigos con control de flujo (bloques `if-else`, `while`, `for`) se pueden formar muchas rutas de ejecución dependiendo de los argumentos que se envíen a la función. Es entonces que se debe evalula todas las rutas que pueda tomar el código.

Por un lado esto se hace con el fin de cubrir todos los posibles comportamientos de la función. Por otro lado, se logra una mejor cobertura de código al evaluar todos los caminos posibles. Se verá cobertura de código más adelante.

Veamos por ejemplo un simple bloque `if` en una versión simplificada de `verifyPersonIsAdult`:

```javascript
export default {
    verifyPersonIsAdult: ({age, isEmancipated}) => {
        const result = {
            isAdult: false,
            isMinor: true
        };

        if (age >= 18) {
            result.isAdult = true;
            result.isMinor = false;
        }

        return result;
    }
};
```

Para este caso podemos encontrar dos flujos de ejecución: el primero se da si se cumple la condición del bloque `if` lo que hará que se ejecuten las sentencias dentro de dicho bloque y luego retornará el resultado. El segundo flujo resultará de no cumplir con la condición del bloque `if` por lo que no se ejecutará el código dentro del bloque, finalizando con el retorno del resultado.

Entonces debemos probar estos dos flujos en nuestras pruebas unitarias:

```javascript
describe('AgeService', () => {
    describe('.verifyPersonIsAdult', () => {
        context('when the person has or is older than 18 years old', () => {
            it('should verify correctly', () => {
                const person = {
                    age: 18
                };

                const result = AgeService.verifyPersonIsAdult(person);

                result.should.have.property('isAdult').which.equals(true);
                result.should.have.property('isMinor').which.equals(false);
            });
        });

        context('when the person is younger than 18 years old', () => {
            it('should not verify', () => {
                const person = {
                    age: 17
                };

                const result = AgeService.verifyPersonIsAdult(person);

                result.should.have.property('isAdult').which.equals(false);
                result.should.have.property('isMinor').which.equals(true);
            });
        });
    });
});
```

A medida que nos encontremos con una mayor cantidad de controles de flujo la cantidad de pruebas unitarias se incrementará. Agreguemos otro control de flujo a `verifyPersonIsAdult`:

```javascript
export default {
    verifyPersonIsAdult: ({age, isEmancipated}) => {
        const result = {
            isAdult: false,
            isMinor: true
        };

        if (age >= 18) {    // primer bloque if
            result.isAdult = true;
            result.isMinor = false;
        }

        if (isEmancipated) {    //segundo bloque if
            result.isAdult = true;
        }

        return result;
    }
};
```

En esta funcionalidad tendremos más flujos de ejecución. Los podemos enumerar de la siguiente manera:

* No se cumple con la condición del primer bloque `if` y tampoco se cumple con la condición del segundo `if` por lo que se retorna directamente el resultado.
* No se cumple con la condición del primer bloque `if` pero  se cumple con la condición del segundo bloque `if` por lo que se ejecuta su código y finaliza retornando el resultado.
* Se cumple con la condición del primer bloque `if` por lo que se ejecuta su código pero no se cumple con la condición del segundo bloque `if` y se retorna el resultado.
* Se cumple con la condición del primer bloque `if` por lo que se ejecuta su código y también se cumple con la condición del segundo bloque `if` por lo que también se ejecuta su código y finaliza retornando el resultado.

Traducido a una tabla de verdad:

|Cumple condición 1er bloque `if`|Cumple condición 2do bloque `if`|¿Verifica?|
|---|---|---|
|No|No|No|
|No|**Si**|**Si**|
|**Si**|No|**Si**|
|**Si**|**Si**|**Si**|

Entonces tenemos cuatro flujos que debemos probar:

```javascript
describe('AgeService', () => {
    describe('.verifyPersonIsAdult', () => {
        context('when the person has or is older than 18 years old', () => {
            context('and it is not emancipated', () => {
                it('should verify correctly', () => {
                    const person = {
                        age: 18,
                        isEmancipated: false
                    };

                    const result = AgeService.verifyPersonIsAdult(person);

                    result.should.have.property('isAdult').which.equals(true);
                    result.should.have.property('isMinor').which.equals(false);
                });
            });

            context('and it is emancipated', () => {
                it('should verify correctly', () => {
                    const person = {
                        age: 18,
                        isEmancipated: true
                    };

                    const result = AgeService.verifyPersonIsAdult(person);

                    result.should.have.property('isAdult').which.equals(true);
                    result.should.have.property('isMinor').which.equals(false);
                });
            });
        });

        context('when the person is younger than 18 years old', () => {
            context('and it is not emancipated', () => {
                it('should not verify', () => {
                    const person = {
                        age: 17,
                        isEmancipated: false
                    };

                    const result = AgeService.verifyPersonIsAdult(person);

                    result.should.have.property('isAdult').which.equals(false);
                    result.should.have.property('isMinor').which.equals(true);
                });
            });

            context('and it is emancipated', () => {
                it('should verify correctly', () => {
                    const person = {
                        age: 17,
                        isEmancipated: true
                    };

                    const result = AgeService.verifyPersonIsAdult(person);

                    result.should.have.property('isAdult').which.equals(true);
                    result.should.have.property('isMinor').which.equals(true);
                });
            });
        });
    });
});
```

Vemos que la cantidad de combinaciones esta directamente relacionado con la cantidad de pruebas unitarias y más precisamente con bloques `it`.

Ahora qué sucede en un bloque `if` que contiene más de una condición a cumplir?

```javascript
export default {
    verifyPersonIsAdult: ({age, isEmancipated}) => {
        const result = {
            isAdult: false
        };

        if (age >= 18 || isEmancipated) {
            result.isAdult = true;
        }

        return result;
    }
};
```

Esta es una versión ampliada de la primer versión de `verifyPersonIsAdult`. Aquí se concentran las dos condiciones en una sola sentencia de nuestro bloque `if`. ¿Cómo serán los flujos en este caso? Vamos a usar la tabla de verdad:

|age >= 18|isEmancipated|¿Verifica?|
|---|---|---|
|false|false|No|
|false|**true**|**Si**|
|**true**|false|**Si**|
|**true**|**true**|**Si**|

Nuestra tabla de verdad es idéntica a nuestro ejemplo anterior dando por resultado que existen cuatro flujos de ejecución por ende nuestras pruebas unitarias serán muy similares (sólo cambian en que no se evalúan los valores de la propiedad `isMinor`).

Entonces podemos deducir que la cantidad de flujo no está directamente relacionada con la cantidad de controles de flujo sino por la cantidad de condiciones que pueden cumplirse o no durante la ejecución del código.

### e. Buenas prácticas

(WIP)

## 4. Evaluando código

### a. Cómo aislar el módulo para pruebas

Puede darse el caso que tendremos que probar código cuya funcionalidad dependa de la funcionalidad de otra pieza de código, sea de una misma clase o de un módulo totalmente ajeno. Al realizar pruebas unitarias debemos aislar el código en cuestión de cualquier funcionalidad que sea externa al código mismo que estamos probando.

Tomemos la siguiente función:

```javascript
import DBService from './DBService';

export default {
    checkStatus: userRole => {
        const {isOperational, currentConnections} = DBService.getMongooseStatus();

        if (userRole === 'guest' || !isOperational) {
            return false;
        }

        if (userRole === 'dba') {
            return currentConnections;
        }

        return true;
    }
};
```

En la función `checkStatus` tenemos una dependencia. Una función que se utiliza del servicio `DBService`. Desde nuestro código no podemos saber con certeza cómo `getMongooseStatus` funciona, sólo que devuelve un objeto del cual obtenemos dos atributos: `isOperational` y `currentConnections`. Al ser un método que evalúa la condición de nuestra base de datos su resultado se dará en base a la condición en el momento que se ejecute. Entonces el resultado de este método, podría decirse, puede no ser siempre el mismo entre ejecuciones.

Al ejecutar pruebas unitarias debemos tener la certeza que el resultado de correr las mismas sea siempre el mismo independientemente del momento que se ejecute o de otros factores que puedan alterar el resultado. Con el fin de evitar esto debemos reemplazar estas funcionalidades ajenas a nuestro código con nueva funcionalidad que nos sea útil y su ejecución de siempre el mismo resultado.

### b. Usando dobles de prueba

Antes de ver de que manera se reemplaza una dependencia debemos saber qué son los doble de prueba; son los que permiten aislar nuestra pieza de código. Un doble de prueba es aquel objeto que reemplaza funcionalidad (por ejemplo, una dependencia) con nueva funcionalidad con el fin de forzar el resultado de derivado de la ejecución del código que se esta probando. Mediante la biblioteca `sinon` podremos usarlos en nuestro proyecto con `Mocha`.

Existen tres tipos de dobles de prueba disponibles con `sinon`:

* `spy` - Permite que se ejecute el código original mientras extiende la funcionalidad con elementos de auditoría. Por regla general no usaremos estos dobles de prueba.
* `stub` - Reemplaza una funcionalidad permitiendonos especificar un nuevo valor de retorno. En esencia es un `spy` que reemplaza el código original.
* `mock` - Es un `stub` que explicita cómo debe ejecutarse un método o cómo espera que se ejecute.

Para reemplazar dependencias utilizaremos los dobles de prueba de `sinon` junto con la biblioteca `proxyquire` para inyectarlas.

### c. Inyección de dependencias

Para poder inyectar una dependencia debemos llamar a `proxyquire` de la siguiente manera:

```javascript
import sinon from 'sinon';
import proxyquire from 'proxyquire';

const getMongooseStatusStub = sinon.stub();

const StatusService = proxyquire('./StatusService', {
    getMongooseStatus: () => getMongooseStatusStub
}).default;
```

Extendiendolo a nuestro ejemplo anterior:

```javascript
import sinon from 'sinon';
import proxyquire from 'proxyquire';

const getMongooseStatusStub = sinon.stub();

const StatusService = proxyquire('./StatusService', {
    getMongooseStatus: () => getMongooseStatusStub
}).default;

declare('StatusService', () => {
    declare('#checkStatus', () => {
        context('when the request is made by a guest', () => {
            it('should deny the request', () => {
                const userRole = 'guest';
                const result = StatusService.checkStatus(userRole);

                result.should.be.false();
            });
        });

        context('when the request is not made by a guest', () => {
            context('and the database is not operational', () => {
                it('should not return the status', () => {
                    getMongooseStatusStub.returns({
                        isOperational: false
                    });
                    const userRole = 'admin';
                    const result = StatusService.checkStatus(userRole);

                    result.should.be.false();
                });
            });

            context('and the user is a dba', () => {
                it('should return the number of current connections', () => {
                    const connectionQuantity = 10;

                    getMongooseStatusStub.returns({
                        currentConnections: connectionQuantity,
                        isOperational: true
                    });

                    const userRole = 'dba';
                    const currentConnections = StatusService.checkStatus(userRole);

                    currentConnections.should.equal(connectionQuantity);
                });
            });

            context('and the user is not a dba', () => {
                it('should return an ok status', () => {
                    getMongooseStatusStub.returns({
                        isOperational: true
                    });

                    const userRole = 'admin';
                    const result = StatusService.checkStatus(userRole);

                    result.should.be.true();
                });
            });
        });
    });
});
```

## 5. Assertions en elementos complejos

### a. Evaluando componentes React

(WIP)
