﻿# Agregados de Babel

He aquí una lista de agregados de Babel y qué es lo que hace cada uno.

## Utilización

Todos los agregados de Babel se pueden encontrar como paquetes con el scope `@babel`. El paquete del agregado se puede obtener desde npm (y también referenciar) agregando el nombre del agregado al scope, anteponiendo la barra al nombre del agregado. Ej.: `@babel/register`.

### Nota sobre plugins

Al referenciar un plugin, se puede omitir `plugin-` del nombre del paquete. Así que es lo mismo hacer una referencia usando `@babel\plugin-proposal-class-properties` o `@babel\proposal-class-properties`.

### Nota sobre presets

Al referenciar un plugin, se puede omitir `preset-` del nombre del paquete. Así que es lo mismo hacer una referencia usando `@babel\preset-react` o `@babel\react`.

## Bibliotecas

Extienden la funcionalidad de un proyecto utilizando una versión específica de ES sin transpilar código.

| Nombre | Descripción |
|--|--|
| `register` | Se utiliza para importar Babel como módulo de RequireJS |
| `polyfill` | Implementa un entorno compatible con ECMAScript 6+ sin necesidad de transpilar. Incorpora un `regenerator-runtime` personalizado y `core-js`.

## Plugins

Conjunto de reglas de transpilación que sirven para interpretar y transpilar código.

| Nombre | Descripción |
|--|--|
| `plugin-proposal-class-properties` | Interpreta miembros de una clase de ECMAScript escritos según propuesta en `stage 3` |
| `plugin-proposal-object-rest-spread` | Interpreta declaraciones `rest` y `spread` para objetos según propuesta de ECMAScript en `stage 4` |
| `plugin-transform-modules-commonjs` | Convierte módulos de ECMAScript a módulos CommonJS |


## Presets

Conjunto de plugins de transpilación.

| Nombre | Descripción |
|--|--|
| `preset-env` | Interpreta código escrito en ECMAScript (no tiene en cuenta características no aprobadas) |
| `preset-react` | Interpreta código escrito en React |
