﻿# Usando paquetes de Babel v6 en Babel v7

Descripción de los paquetes principales de Babel:

* `@babel\core` - Paquete de Babel v7. No compatible con paquetes que utilicen Babel v6.
* `babel-core` - Paquete de Babel v6. Compatible con versiones anteriores de Babel.
* `babel-core@^7.0.0-bridge.0` - Paquete de transición de Babel v6 para ser utilizado en entornos configurados con Babel v7. Permite utilizar paquetes hechos con Babel v6.

## Utilización

Se deben incorporar ambos paquetes `core` en la aplicación:

```sh
npm install --save-dev babel-core@^7.0.0-bridge.0 @babel/core
```

**Deben descargarse ambas dependencias, de lo contrario Babel fallará al traspilar los archivos y la operación quedará trunca.**
