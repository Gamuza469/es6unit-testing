# Aserciones con Chai

## Tipo de variable

Se deben utilizar los métodos `a` o `an` y se le deben pasar por parámetro una cadena con el tipo de variable:

```javascript
it('should check variable type', () => {
    const integer = 4;
    integer.should.be.a('number');

    const function = () => _.noop();
    function.should.be.a('function');

    const object = {};
    object.should.be.an('object');
});
```

Los tipos que se pueden corroborar son:

- `number` o `Number` (si se trata de un objeto Number)
- `string` o `String` (si se trata de un objeto String)
- `Date`
- `Array`
- `Object`
- `Promise`
- `instanceof(Class)`
- `function`
- Demás tipos introducidos en ES6 (`Map`, `Set`, `Int8Array`, etc.)

## Valor de una variable

### Valores específicos

```javascript
it('should check values', () => {
    const number = 5;
    number.should.equal(5);

    const string = 'it is a string';
    string.should.equal('it is a string');

    const boolean = true;
    boolean.should.be.true;

    const boolean2 = false;
    boolean2.should.be.false;
});
```

### Variables vacías

Cadenas y `arrays` se consideran vacíos si su atributo `length` es cero:

```javascript
it('should check empty variables', () => {
    const string = '';
    string.should.be.empty;

    const array = [];
    array.should.be.empty;
});
```

Al evaluar objetos, se considera vacíos aquellos objetos que tengan una cantidad de `keys` igual a cero.

```javascript
it('should check empty variables', () => {
    const object = '';
    object.should.be.empty;
});
```

## Atributos de un objeto

### Existencia de atributos

Cuando debemos comprobar que existan atributos que son requeridos, entre muchos otros que no lo son. Util cuando sólo debemos comprobar que existen una cantidad de atributos de un objeto que puede contener otros atributos.

```javascript
it('should check properties', () => {
    const bicycle = {
        speed: 80,
        gears: 2,
        brakeType: 'disc'
    };

    bicycle.should.have.property('brakeType');
    bicycle.should.have.property('speed');
    bicycle.should.have.property('gears');
});
```

### Existencia de atributos con valores específicos

Cuando debemos comprobar que existan atributos que son requeridos y que cada atributo tenga el valor pedido, entre muchos otros atributos irrelevantes. Útil cuando sólo debemos comprobar que existen una cantidad de atributos, con valores necesariamente específicos, de un objeto que puede contener otros atributos.

```javascript
it('should check properties', () => {
    const car = {
        wheels: 4,
        trunk: true,
        traction: '4wd',
        breakType: 'disk',
        maxSpeed: '300'
    };

    // Check one specific property
    car.should.have.property('traction').which.equal('4wd');

    // Check several properties
    car.should.deep.equal({
        wheels: 4,
        trunk: true
    });
});
```

### Comparación estricta de objetos

En el caso que necesitemos corroborar que un objeto tiene los atributos requeridos y ninguno más, debemos utilizar una comparación estricta de objetos:

```javascript
it('should check the object properties', () => {
    const member = {
        name: 'John Doe',
        age: 30,
        money: 3039242,
        takesSurvey: true
    };

    const desiredMember = {
        age: 30,
        takesSurvey: true,
        money: 3039242,
        name: 'John Doe'
    };

    member.should.deep.equal(desiredMember);

    const sameMember = member;
    member.should.deep.equal(sameMember);
});
```

**Nota:** `equal`, al ser utilizado con `deep` evalúa a verdadero si los objetos comparados tienen los mismos atributos con los mismos valores, indistintamente del orden de los atributos o si tiene la misma referencia al objeto.

## Tipo de atributo

En el caso que sólo necesitamos comprobar el tipo de un atributo o resultado de una operación. Útil en el caso que sólo necesitamos corroborar el tipo de atributo del retorno sin necesidad de conocer su valor/contenido.

```javascript
it('should check attribute type', () => {
    const household = {
        members: [
            {
                name: 'John',
                surname: 'Doe',
                age: 30
            }
        ]
    };

    household.should.have.property('members').which.is.an('Array');
});
```

```javascript
it('should check attribute type', () => {
    const households = [
        {address: 'False Avenue 123'},
        {houseType: 'Apartment building'}
    ];

    households.should.be.an('Array');
});
```

## Evaluando *null* y *undefined*

```javascript
const chai = require('chai');
const should = chai.should();

it('should check the null', () => {
    const nullValue = null;
    const undefinedValue = undefined;

    // Check null value
    should.equal(nullValue, null);

    // Check undefined value
    should.equal(undefinedValue, undefined);

    // Throws an error
    nullValue.should.be.null;
    // Throws an error
    undefinedValue.should.be.undefined;
});
```

Nótese que debemos requerir el paquete `should` explícitamente para poder utilizarlo como método.

## Excepciones y errores

Se pueden hacer `assertions`  para corroborar que se lanzan excepciones o errores cuando se lo necesita. Para verificar que un método lanza una excepción, debemos crear un método que ejecute el método sobre el cual queremos verificar si se lanza correctamente la excepción:

### Verificar lanzamiento de una excepción

```javascript
it('should check for exceptions', () => {
    const throwsException = () => throw new Error();

    (() => throwsException()).should.throw();
});
```

### Verificar lanzamiento de una excepción desde un bloque try/catch

Algunas piezas de código pueden consistir en bloques `try/catch` que atrapan posibles excepciones de diferentes métodos, ejecutan código y luego lanzan esa excepción. Para ello debemos emplear un caso similar:

```javascript
it('should check for the exception', () => {
    const executesStuff = sinon.stub();
    const throwsException = () => throw new SyntaxError('Bad syntax');

    const shouldBeCaught = () => {
        try {
            throwsException();
        } catch (error) {
            executesStuff();
            throw error;
        }
    };

    try {
        shouldBeCaught();
    } catch (caughtError) {
        sinon.assert.calledOnce(executesStuff);

        caughtError.should.be.an.instanceof(Error);
        caughtError.should.have.property('name').which.equal('SyntaxError');
        caughtError.should.have.property('message').which.equal('Bad syntax');
    }
});
```

### Verificar lanzamiento de una excepción desde un bloque try/catch, a partir de un método asincrónico

Siguiendo a partir del ejemplo de arriba, generalmente no se necesita corroborar que `caughtError` sea un objeto `Error`. Sin embargo, si estamos ejecutando un método asincrónico con `async/await` y ese método devuelve un `Promise.reject`, lo que sea atrapado en el bloque `catch` debe ser un objeto que sea una instancia de `Error`.

```javascript
it('should check for the exception', async () => {
    const executesStuff = sinon.stub();
    const throwsException = () => Promise.reject(new Error('Promise has failed'));

    const shouldBeCaught = async () => {
        try {
            await throwsException();
        } catch (error) {
            executesStuff();
            throw error;
        }
    };

    try {
        await shouldBeCaught();
    } catch (caughtRejection) {
        sinon.assert.calledOnce(executesStuff);

        caughtRejection.should.be.an.instanceof(Error);
        caughtRejection.should.have.property('message').which.equal('Promise has failed');
    }
});
```

## Promesas

### Pruebas sobre el resultado de un método asincrónico

Trabajar con el resultado de un método asincrónico a base de `Promises` se vuelve sencillo en el caso de promesas resueltas.

```javascript
it('should check the result', async () => {
    const resolvedPromise = () => Promise.resolve({
        status: 'ok',
        health: 95,
        lastRevisor: 'johndoe'
    });

    const result = await resolvedPromise();

    result.should.be.an('Object')
        .and.have.property('health')
        .which.is.a('number')
        .and.equal(95);
});
```

A la hora de tratar con rechazos, nuestra prueba se torna un poco más programática que coloquial.

```javascript
it('should check the rejection', async () => {
    const rejectedPromise = () => Promise.reject(
        new Error('Promise was rejected')
    );

    try {
        await rejectedPromise();
    } catch (error) {
        error.should.be.an.instanceof(Error)
            .and.have.property('message')
            .which.equal('Promise was rejected');
    }
});
```

Al retornar un rechazo utilizando `async/await`, este se toma siempre como un error y por ende terminará la ejecución a menos que se atrape con un bloque `try/catch`.

### Verificar, con detalle, la resolución de una promesa utilizando `chai-as-promised`

A diferencia de las pruebas unitarias directamente sobre el resultado de una promesa, podremos trabajar con su resultado utilizando la palabra `eventually` seguido de las `assertions`.

>**Nota**
>Para poder utilizar estos métodos se debe utilizar la biblioteca `chai-as-promised`.

```javascript
it('should check the promise', async () => {
    const resolvedPromise = new Promise(resolve => resolve({
        status: 'ok',
        health: 95,
        lastRevisor: 'johndoe'
    }));

    // Check promise
    await resolvedPromise.should.eventually.be.an('Object')
        .and.have.property('health')
        .which.is.a('number')
        .and.equal(95);
});
```

### Corroborar una promesa resuelta

Podemos corroborar directamente el valor de la resolución de una promesa.

```javascript
    it('should check the promise resolution', async () => {
    const returnResolvedPromise = Promise.resolve(10);

    // Check resolution
    await returnResolvedPromise.should.be.fulfilled;

    // Check resolution and value returned
    await returnResolvedPromise.should.become(10);
});
```

### Corroborar una promesa rechazada

```javascript
it('should check the promise rejection', async () => {
    const returnRejectedPromise = Promise.reject(new Error());

    // Check rejection
    await returnRejectedPromise.should.be.rejected;

    // Check rejection and value returned
    await returnRejectedPromise.should.be.rejectedWith(Error);
});
```
