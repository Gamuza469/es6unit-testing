﻿# Aserciones con Should-Sinon

El paquete `should-sinon` nos permite realizar `assertions` sobre los `spies` y `stubs` de modo que queden de una forma semántica.

La biblioteca **Sinon.js** ya incluye herramientas para hacer `assertions` sobre `spies` y `stubs`. Los métodos aquí descriptos pueden ser llamados desde `sinon.assert` y son tanto homónimos como idénticos:

```javascript
const executedCode = sinon.stub();
executeCode(1, '2', [3]);

// should-sinon
executeCode.should.have.been.called():
// sinon.assert
sinon.assert.called(executeCode);
```

## Verificar llamada a método con argumentos

### Verificación estricta

Verifica que el `spy` o `stub` haya sido llamado con todos los argumentos requeridos.

```javascript
it('should check the call', () => {
    const executeCode = sinon.stub();
    executeCode(1, '2', [3]);
    executeCode.should.have.been.calledWithExactly(1, '2', [3]);
});
```

### Verificación no estricta

No es necesario que los argumentos sean referencias, únicamente verifica que los valores sean iguales.

Al hacer la verificación, los argumentos tienen que respetar el orden en el que se describe en el `assertion`.

```javascript
it('should check the call', () => {
    const executeCode = sinon.stub();
    executeCode(1, '2', [3]);

    // Check some arguments
    executeCode.should.have.been.calledWith(1, '2');
    // Check all arguments
    executeCode.should.have.been.calledWith(1, '2', [3]);
});
```

## Cantidad de llamadas a la función

```javascript
it('should check the call count', () => {
    const executeCode = sinon.stub();

    executeCode();
    // Check if called only once
    executeCode.should.have.been.calledOnce();

    executeCode();
    // Check if called twice
    executeCode.should.have.been.calledTwice();

    executeCode();
    // Check if called three times
    executeCode.should.have.been.calledThrice();

    executeCode();
    executeCode();
    // Check if called a specific amount of times
    executeCode.should.have.been.callCount(5);
});
```

## Verificación sobre excepciones

```javascript
it('should check the exception', () => {
    const executeCode = sinon.stub().throws();

    try {
        executeCode();
    } catch (error) {
        executeCode.should.threw();
    }
});
```

Sin embargo, el `assertion` no queda bien semánticamente. Se puede corregir el mismo utilizando los `alias` de **Sinon.js**

```javascript
should.Assertion.alias('threw', 'thrown');

it('should check the exception', () => {
    const executeCode = sinon.stub().throws();

    try {
        executeCode();
    } catch (error) {
        executeCode.should.have.thrown();
    }
});
```

La manera de evaluar es distinta porque los `spies` y `stubs` registran si el método lanzó una excepción, a diferencia de los métodos tradicionales. Aunque si lo quisieramos podemos evaluar de la manera convencional:

```javascript
it('should check the exception', () => {
    const executeCode = sinon.stub().throws();

    (() => executeCode()).should.throw();
});
```
