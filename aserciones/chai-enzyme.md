﻿# Aserciones con Chai-Enzyme

## Verificar *state* actual

```javascript
it('should check the state', () => {
    const myComponent = shallow(<MyComponent />);
    myComponent.setState({
        name: 'bar',
        surname: ''
    });

    myComponent.should.have.state('name').which.equal('bar');
    myComponent.should.have.state('surname').which.is.empty;
});
```

## Verificar cantidad de componentes

```javascript
it('should check the amount of components', () => {
    const myComponent = mount(<MyComponent />);

    myComponent.should.have.exactly(1).descendants(MyOtherComponent);
    myComponent.should.have.exactly(4).descendants('div');
    myComponent.should.have.exactly(1).descendants('input[name="username"]');
});
```

> **Nota**
Sólo para este caso, debe usarse ambos `assertions` a la vez en el orden descripto.

## Verificar las *props* de un componente React

No se debe confundir con atributos **HTML**.

### Verificando una única `prop`

```javascript
it('should check the component prop', () => {
    const myComponent = shallow(<MyComponent />);

    myComponent.should.have.prop('users').which.equal(5);
});
```

### Verificando varias `prop`

```javascript
it('should check the component prop', () => {
    const myComponent = shallow(<MyComponent />);

    myComponent.should.have.props(['users', 'upTime']).which.equal({
        users: 5,
        upTime: 109291023
    });
});
```

## Verificando el estado de un elemento HTML

### Verificar texto de un componente

```javascript
it('should check the component\'s text', () => {
    const myComponent = shallow(<MyComponent />);
    const button = myComponent.find('button');
    button.should.have.text('Guardar');
});
```

> **Nota**
En el caso que el texto tenga 'nbsp', se deben agregar explícitamente al texto del `assertion`.

```html
<button> Iniciar sesión  </button>
```

```javascript
button.should.have.text('\u00a0Iniciar sesión\u00a0\u00a0');
```

## Verificar el atributo de un componente HTML

No se debe confundir con `props`.

```javascript
it('should check the component attribute', () => {
    const myComponent = shallow(<MyComponent />);
    const input = myComponent.find('input');

    input.should.have.attr('name').which.equal('username');
});
```

## Verificar si un *checkbox* fue tildado

```javascript
it('should verify the input is checked', () => {
    const myComponent = shallow(<MyComponent />);
    const input = myComponent.find('input');

    input.should.be.checked();
});
```
