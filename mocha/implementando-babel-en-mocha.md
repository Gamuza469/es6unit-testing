# Implementando Babel en Mocha

**Mocha** no soporta código según el estandar **ECMAScript2015 (ES6)**. Si se utiliza código **ES5** y se importan módulos mediante **Common.js** (utilizando `require`) no debería haber problemas. En un entorno en donde se utilizan `import/export` o propiedades `rest/spread`, donde son funcionalidades de **ES6** en adelante o son `funcionalidades propuestas`, **Mocha** no será capaz de reconocerlas. Para poder transpilar ese código a **ES5** necesitamos usar **Babel.js**.

## Dependencias

Necesitamos agregar los paquetes propios de **Babel.js** para poder realizar la transpilación:

```sh
npm install --save-dev @babel/core @babel/register
```

### Paquete principal

- `@babel/core` - Paquete principal de `Babel.js`, versión 7.

### Paquete secundario

- `@babel/register` - Permite transpilar código de manera programática.

## Configuración Mocha

Debemos agregar a nuestro archivo de inicialización las siguientes líneas:

>**Nota**
>En nuestro caso, el archivo de inicialización se encuentra en **/test/setup.js**

```javascript
require('@babel/register')({
    ignore: [/node_modules\/(?!react-native|react-router-native)/]
});
```

Lo que hace esta declaración es **no** transpilar todos los paquetes de `node_modules` a excepción de los paquetes `react-native` y `react-router-native`.

Parafraseando, se transpilarán todos los archivos del proyecto y únicamente dos paquetes de la carpeta de `node_modules`, que son `react-native` y `react-router-native`. Los demás paquetes de `node_modules` no serán transpilados.

Si se desean agregar paquetes **npm** que se desean transpilar, se deben agregar dentro del paréntesis separado por un `pipe` (barra vertical).

>**Ejemplo**
>Para agregar el paquete **lodash** a la lista de paquetes **npm** a transpilar:

```javascript
require('@babel/register')({
    ignore: [/node_modules\/(?!react-native|react-router-native|lodash)/]
});
```

## Ejecutando Mocha

En el momento que se ejecuta **Mocha** se transpilarán los archivos del proyecto, los paquetes `npm` necesarios y los archivos de pruebas. Luego de la transpilación de los archivos a **ES5** se procederá a ejecutar las pruebas de manera normal.
