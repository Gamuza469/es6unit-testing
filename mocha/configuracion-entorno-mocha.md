# Configuración de un entorno de pruebas con Mocha

## Dependencias

Debemos instalar los siguientes paquetes para poder ejectuar nuestro entorno de prueba:

```sh
npm install --save-dev mocha should
```

### Paquetes principales

- `mocha` - El `framework` de pruebas a utilizar.
- `should` - Biblioteca que se encargará de manejar los `assertions`.

## Definiendo el script

Para poder ejecutar el entorno de prueba podemos configurar el archivo `package.json` para que corra **Mocha** junto con las pruebas unitarias.

>**package.json**

```json
{
    "name": "Project",
    "scripts": {
        "start": "node index.js",
        "test": "mocha"
    },
    "dependencies": {...}
}
```

De esta manera, podremos iniciar la ejecución escribiendo en nuestra *CLI*:

```sh
npm test
```

Podemos agregar el comando `clear;` adelante para que limpie la consola antes de ejecutar las pruebas.

## Configuración Mocha

Por defecto, **Mocha** buscará el archivo de configuración `mocha.opts` en la carpeta `/test`. Este archivo junta todos los argumentos necesarios que normalmente se pasarían por *CLI*.

>**/test/mocha.opts**

```sh
--require ./test/setup
./test/**/*.spec.js
```

La primer línea importa módulos y los ejecuta al iniciar el *framework*. La segunda línea especifica, mediante *GLOB patterns*, los archivos que contienen las pruebas a ejecutarse.

## Archivo de inicialización

Como vimos antes, le pedimos a **Mocha** que importara el archivo `/test/setup.js`. Este archivo se encargará de importar los módulos necesarios y ejecutar código de configuración adicional, sea de **Mocha** o de alguna otra parte del entorno de pruebas.

>**/test/setup.js**

```javascript
process.env.NODE_ENV = 'test';
```

En nuestro caso, sólo le pediremos que configure el parámetro `env` a `test`.
