﻿# Configuración básica

Es necesario importar los siguientes módulos para poder utilizar los métodos de Enzyme en nuestras pruebas:

```javascript
import enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({adapter: new Adapter()});
```

En el caso de utilizar Chai y la biblioteca de afirmaciones `chai-enzyme` se debe configurar Chai de la siguiente manera:

```javascript
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());
```

Estas sentencias las podemos ejecutar automáticamente al iniciar Jest. Para ello se deben realizar las importaciones desde un archivo y luego configurar Jest desde el archivo `jest.config.js` y hacer la referencia:

> **/tests/setup.js**

```javascript
import enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

enzyme.configure({adapter: new Adapter()});
```

> **/jest.config.js**

```javascript
module.exports = {
    setupTestFrameworkScriptFile: '<rootDir>/tests/setup.js',
};
```
