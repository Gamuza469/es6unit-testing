﻿# Concepto de prueba unitaria

`Unit testing` involucra la prueba de una unidad de código. En este sentido debemos minimizar o eliminar toda funcionalidad que afecte directamente el flujo del código que necesitamos probar. Dichas funcionalidades son agentes externos al código. Estas pueden ser métodos de una misma clase, bibliotecas, llamadas asincrónicas (por `AJAX` o eventos temporales), manipulación de archivos, etcétera.

Toda aquella llamada a funcionalidad externa al código a evaluar debe ser eliminada. La primera razón es que desconocemos cómo esas funcionalidades pueden actuar a la hora de ejecutarlas por distintos factores como pueden ser su propio flujo de código, que puede mutar en un momento del ciclo de vida de la aplicación y dar un resultado distinto para los mismos `inputs`; intervalos de tiempo que en tiempo de ejecución pueden variar; llamadas `HTTP` a servidores a los cuales no podemos acceder y con las cuales no podemos probar correctamente nuestro `test` si no tenemos al respuesta a la misma.

Estos elementos externos, sensibles a comportamientos que no podemos manejar o que su funcionamiento no son 100% previsibles influyen en el éxito o fracaso de nuestra prueba.

La segunda razón, igual de importante que la primera, es el hecho que al probar diferentes piezas de código relacionadas estaríamos desarrollando una prueba de integración o `integration test`. En estas pruebas se corrobora que el comportamiento de dos o más piezas de código se da de manera correcta.

Una vez aislada la pieza de código, con los agentes externos controlados de tal manera que la ejecución del código se realice de manera determinista, estaremos ante un `unit test` genuino, el cual evaluará de la misma manera toda y cada vez que este se ejecute dado que no esta influenciado por agentes externos.
