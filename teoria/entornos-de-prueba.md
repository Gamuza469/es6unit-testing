﻿# Entornos de pruebas unitarias

Cada proyecto debe tener una solución de entorno de pruebas unitarias que se adapte a su contexto. No es posible utilizar la misma receta para todos los proyectos pero se pueden determinar entornos para proyectos que cumplen ciertas condiciones. En nuestro caso, podemos encontrar los siguientes entornos de prueba según que tecnologías manejen nuestros proyectos:

## React

- **Jest** como `framework`, que incluye herramientas de `test doubles`.
- **Babel** para transpilar nuestro código ES6.
- **Enzyme** para probar componentes de **React**.
- **Chai** como biblioteca de `assertions`.
- **Chai-Enzyme** como biblioteca de `assertions` para `wrappers` de **Enzyme**.

## Node.js

- **Mocha** como `framework`.
- **Sinon.js** como herramienta de `test doubles`.
- **Should.js** como biblioteca de `assertions`.

## AngularJS

- **Karma** como entorno de ejecución de pruebas.
- **Mocha** como `framework`.
- **Sinon.js** como herramienta de `test doubles`.
- **Chai** como biblioteca de `assertions`.
- **Sinon.js-Chai** como biblioteca de `assertions` para `test doubles` de **Sinon.js**.

## General (wip)

- **Mocha** como `framework`.
- **Babel** para transpilar nuestro código ES6.
- **Enzyme** para probar componentes de **React**.
- **Sinon.js** como herramienta de `test doubles`.
- **Should.js** como biblioteca de `assertions`.
- **Should.js-Sinon** como biblioteca de `assertions` para `test doubles` de **Sinon.js**.
- **Should.js-Enzyme** como biblioteca de `assertions` para `wrappers` de **Enzyme**.
