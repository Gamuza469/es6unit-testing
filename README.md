# Unit-testing on ECMAScript 6

## Lista de contenidos

1. Aprendizaje
   1. [Instructivo](./instructivo/README.md)
   2. [Teoría](./teoria/README.md)
   3. [Aserciones](./aserciones/README.md)
   4. [Aplicación](./tests/README.md)
2. Frameworks
   1. [Mocha](./mocha/README.md)
   2. [Jest](./jest/README.md)
3. Tecnologías
   1. [Enzyme](./enzyme/README.md)
   2. [Sinon.js](./sinon/README.md)
4. Otros
   1. [Babel](./babel/README.md)

## Instructivo

Para comprender las ideas básicas de pruebas unitarias y aprender cómo montar un entorno de pruebas con **Mocha** se puede acceder a nuestro [instructivo](./instructivo/README.md).

## Referencia

Listado de `frameworks`, bibliotecas, tecnologías, etc. y vínculos a documentación sobre APIs, métodos y demás:

- **Enzyme** ([link](https://airbnb.io/enzyme/docs/api/))
- **Sinon.js** ([link](https://sinonjs.org/releases/latest/))
  - [Spies](https://sinonjs.org/releases/latest/spies/)
  - [Stubs](https://sinonjs.org/releases/latest/stubs/)
  - [Mocks](https://sinonjs.org/releases/latest/mocks/)
  - **sinon.assert** ([link](https://sinonjs.org/releases/latest/assertions/))
- **Should.js** ([link](https://shouldjs.github.io))
  - **should-sinon** ([link](https://github.com/shouldjs/sinon))
  - **should-enzyme** ([link](https://github.com/rkotze/should-enzyme))
- **Chai** ([link](https://www.chaijs.com/api/bdd/))
  - **chai-enzyme** ([link](https://github.com/producthunt/chai-enzyme))
- **Jest**
  - [Mocks](https://jestjs.io/docs/en/mock-functions.html)
  - [Dependency mocks](https://jestjs.io/docs/en/es6-class-mocks)
