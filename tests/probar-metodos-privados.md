﻿# Cómo testear métodos o atributos no exportados

## Nota sobre métodos privados (Javascript)

En el caso de los métodos privados en Javascript es posible acceder a ellos y probarlos de manera unitaria en ciertas ocasiones.

El primer caso es el descrito arriba, un método importado, externo a la clase pero a fin de cuentas privado para la misma. Este método privado sólo se lo puede acceder mediante otro método, que en este caso sería un método público. Este caso es fácil de manejar.

El segundo caso se da cuando se crea un método dentro de la clase a importar pero que el mismo no ha sido exportado correctamente, manteniendo la privacidad del método al limitar el contexto a la clase.  Aquí no tenemos manera de probar directamente nuestro código ya que no podemos acceder a ese método para crear un `stub`. Podemos solventar este problema con bibliotecas externas como lo son `proxyquire` o `rewire` para importar el método, modificar el método y/u otros atributos y colocar nuestro `stub`.

El tercer caso, el más molesto de todos, es cuando el método se define dentro de nuestra clase y es llamado por uno de los métodos de la clase. Este método privado puede ser definido, por ejemplo, en el constructor. Dado que estamos ante un caso de `closure`, por la forma que funciona `Javascript` no hay forma de poder reescribir los métodos `privados`. Las herramientas `proxyquire` y `rewire` no son capaces de reescribir o acceder a estos métodos de ninguna manera.

En el caso que sea posible, para poder realizar pruebas de código unitarias sobre estos métodos privados se recomienda crear estos métodos en una nueva clase e importarlos a la clase que los utilizará. Así nuestra clase mantiene la privacidad de los métodos que necesitan serlo y además permite tener nuestro código limpio y permeable a pruebas unitarias tanto en el contexto público como el privado.

Para comprobar esto, tengamos en cuenta el siguiente archivo:

>**Person.js**

```javascript
const getStatus = (name, age) => ({name, status: 'ok', madurity: age >= 18 ? 'young' : 'child'});

class Person {
    constructor (name, age) {
        this.status = 'undefined';
        this.name = name;
        this.age = age;
    }

    initialize () {
        return getStatus(this.name, this.age);
    }
}

module.exports = Person;
```

Deseamos generar `unit tests` para esta clase. Para ello creamos nuestra suite de tests:

>**Person.spec.js (1)**

```javascript
require('should');

const Person = require('./Person');

describe('Person', () => {
    describe('.initialize', () => {
        it('should get the person\'s status', () => {
            const person = new Person('John', 21);
            const status = person.initialize();
            status.should.have.properties(['name', 'status', 'madurity']);
        });
    });
});
```

Nuestra suite terminaría aquí porque no hay otros flujos que probar en el método `initialize` ni otros métodos en la clase. Sin embargo, si la cobertura del código o `coverage` derivado de nuestros tests nos mostraría que el método utilizado por `initialize`,  `getStatus`,  no estaría cubriendo el caso `else` de nuestro operador condicional/ternario (en otras palabras, no se está probando el código para el caso que el método deba pasar por el caso de falsedad del operador ternario de `madurity`).

La primer solución que se nos ocurre sería crear un segundo test que pruebe el código para una persona menor de 18 años:

>**Person.spec.js (2)**

```javascript
require('should');

const Person = require('./Person');

describe('Person', () => {
    describe('.initialize', () => {
        it('should get the person\'s status when is older than 18', () => {
            const person = new Person('John', 21);
            const status = person.initialize();
            status.should.have.properties(['name', 'status', 'madurity']);
            status.madurity.should.equal('young');
        });

        it('should get the person\'s status when is younger than 18', () => {
            const person = new Person('Maria', 16);
            const status = person.initialize();
            status.should.have.properties(['name', 'status', 'madurity']);
            status.madurity.should.equal('child');
        });
    });
});
```

Esto nos permitiría alcanzar el 100% de cobertura de código. Sin embargo, esta aproximación es errónea.

Si nos fijamos bien, lo que deberíamos probar del método `initialize` no es el contenido del objeto que retorna, sino su estructura. La única responsabilidad del método `initialize` es la de retornar un objeto con las propiedades `name`, `status` y `madurity`, no tiene ninguna responsabilidad de manipular los valores de esas propiedades. Quien se encarga de manipular eso datos realmente es el método `getStatus`. Entonces nuestro código debería quedar como el caso **Person.spec.js (1)**. El único problema es que no tenemos manera alguna de acceder a dicho código dado que, como vemos en **Person.js**, solamente exportamos la clase `Person` pero no el método `getStatus` y por ende no podemos generar los tests para probar ese método.

Para estos casos, existen bibliotecas especializadas en reemplazar funciones o variables definidas de manera privada o que no hayan sido exportadas. La más conocida es **Rewire**.

## Reemplazando funciones utilizando Rewire ([GitHub](https://github.com/jhnns/rewire))

Esta biblioteca nos permite acceder a los métodos privados de un archivo y modificarlos a gusto para que estos nos sirvan para probarlos o crear `mocks` para probar funcionalidad que dependa de ellos. Para ver un ejemplo sobre cómo se utiliza `rewire` se puede visualizar el siguiente [video](https://www.youtube.com/watch?v=KMUuDqJ4RnU) en [YouTube](www.youtube.com).

> **Advertencia**

Dicho esto, se desaconseja su uso. Mucha de la funcionalidad que provee **Rewire** puede reemplazarse por la configuración/inicialización correspondiente del entorno de pruebas, un diseño del proyecto o estructura del código mas modularizada o utilizando un inyector de dependencias como es **Proxyquire**. Lo que nos provee **Rewire** lo podríamos considerar como una solución temporal a problemas que deben ser resueltos al corto o mediano plazo.

Ahora podremos probar el método `getStatus` sin ningún problema:

>**Person.spec.js (3b)**

```javascript
require('should');
const rewire = require('rewire');
const Person = require('./Person');
const personMock = rewire('./Person');

describe('Person', () => {
    describe('.initialize', () => {
        it('should get the person\'s status', () => {
            const person = new Person('John', 21);
            const status = person.initialize();
            status.should.have.properties(['name', 'status', 'madurity']);
        });
    });

    describe('.getStatus', () => {
        const getStatus = personMock.__get__('getStatus');
        const name = 'Mario';

        it('should return an object with \'madurity\' for \'young\' people', () => {
            const correctOutput = {
                name,
                status: 'ok',
                madurity: 'young'
            };

            getStatus(name, 18).should.deepEqual(correctOutput);
            getStatus(name, 20).should.deepEqual(correctOutput);
        });

        it('should return an object with \'madurity\' for \'child\' people', () => {
            getStatus(name, 17).should.deepEqual({
                name,
                status: 'ok',
                madurity: 'child'
            });
        });
    });
});
```

De esta manera obtenemos 100% de cobertura y nuestras pruebas son consistentes y objetivas pero con un ligero detalle. El test del método `initialize` no es unitario sino de integración o  un `integration test`. Esto se debe al hecho que `initialize` depende de `getStatus`, volviéndolo un `integration test`. Podremos cambiar esto si creamos un `mock` de `getStatus` para nuestro test de `initialize`. Además, podemos probar que el método `getStatus` fue llamado desde `initialize` utilizando la biblioteca `sinon`:

>**Person.spec.js (4b)**

```javascript
require('should');
const sinon = require('sinon');
const rewire = require('rewire');
const Person = require('./Person');
const personMock = rewire('./Person');

describe('Person', () => {
    describe('.initialize', () => {
        it('should get the person\'s status', () => {
            const getStatusStub = sinon.stub().returns(
                {name: 'Joseph', status: 'ok', madurity: 'young'}
            );
            const revertGetStatus = personMock.__set__('getStatus', getStatusStub);

            const person = new Person();
            const status = person.initialize();
            sinon.assert.calledOnce(getStatusStub);
            status.should.have.properties(['name', 'status', 'madurity']);

            revertGetStatus();
        });
    });

    describe('.getStatus', () => {
        const getStatus = personMock.__get__('getStatus');
        const name = 'Mario';

        it('should return an object with \'madurity\' for \'young\' people', () => {
            const correctOutput = {name, status: 'ok', madurity: 'young'};

            getStatus(name, 18).should.deepEqual(correctOutput);
            getStatus(name, 20).should.deepEqual(correctOutput);
        });

        it('should return an object with \'madurity\' for \'child\' people', () => {
            getStatus(name, 17).should.deepEqual({name, status: 'ok', madurity: 'child'});
        });
    });
});
```

De esta manera cubrimos todos los posibles escenarios con una cobertura del 100% además de asegurar que todo nuestro código se encuentra desacoplado de otras dependencias, asegurando que nuestra suite esta compuesta de tests unitarios.

## Acerca de las pruebas unitarias en métodos privados

La primera respuesta a la pregunta: ¿se deben realizar pruebas unitarias a los métodos privados?, es `no`. Los métodos privados ayudan al encapsulamiento y por ende, no es necesario su prueba. Una respuesta auxiliar es el argumento de lograr una cobertura del código del 100% no es realista, como para hacernos desistir de realizar las pruebas. Finalmente, si queremos realmente llegar a una cobertura total o probar nuestros métodos privados, debemos hacerlo indirectamente, a través de los métodos públicos que lo utilizan.

Aunque esto sea así, muchas veces los métodos privados contienen lógica que es relevante para nuestro sistema y por tanto deben ser probados. La relevancia dependerá de cada sistema, la lógica de los métodos y el impacto en el sistema debido al comportamiento no comprobado por nuestras pruebas. Si el código es relevante se recomienda exportar la funcionalidad y luego ser importada en donde sea necesario (en una clase, por ejemplo), haciendo posible el uso de pruebas unitarias sobre dicho código.
