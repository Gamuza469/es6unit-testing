# Reemplazando dependencias para pruebas unitarias

En los proyectos de software es muy común encontrarse con clases o métodos que dependen de la funcionalidad que proveen otros módulos, tanto de propia autoría como hechos por terceros. Estas dependencias son necesarias para que nuestro código funcione e incluso mejore su eficiencia. A la hora de realizar pruebas unitarias sin embargo se debe prescindir de su utilización de manera tal que el único código que se ejecute y se evalúe sea el módulo de código en cuestión.

## Reemplazando dependencias con Proxyquire ([GitHub](https://github.com/thlorenz/proxyquire))

[video](https://www.youtube.com/watch?v=1XTMStnVV3U)

Esta herramienta nos permite modificar las dependencias de una clase al momento de la importación de modo tal que se puedan reemplazar dependencias con código acorde a nuestra necesidad. En nuestro caso nos permitirá reemplazar métodos o módulos completos por dobles de prueba, los cuales serán `stubs` en su mayoría.

Consideremos los siguientes archivos:

>**Util.js**

```javascript
module.exports = {
    sumTwoValues: (a, b) => a + b
};
```

>**Account.js**

```javascript
const {sumTwoValues} = require('Util');

class AccountService {
    getAverage (firstAccountBalance, lastAccountBalance) {
        return sumTwoValues(firstAccountBalance, lastAccountBalance) / 2;
    }
}

module.exports = AccountService;
```

Se puede apreciar que en **Account.js** tenemos una dependencia del módulo **Util**. La dependencia se traduce en el uso del método `sumTwoValues` dentro del método `getAverage`. Es necesario realizar las pruebas unitarias pertinentes sobre la funcionalidad de `getAverage`. Por ende, necesitamos aislar `getAverage` de sus dependencias.

**Proxyquire** tiene la particularidad de poder importar módulos y modificarlos a gusto sin modificar la implementación original, haciendo posible el importar módulos impolutos por el uso de **Proxyquire** en el mismo contexto de trabajo.

Como vemos en el código, la función `sumTwoValues` es externa a `getAverage` y por ende debemos generar un doble de pruebas que la reemplace con un funcionamiento predecible y conocido. Para poder crear un `stub` y que `getAverage` sea capaz de utilizarlo debemos hacer que el módulo que lo contiene (**Account.js**) importe nuestro `stub` en vez del código original. Para ello debemos importar al módulo de la siguiente manera:

>**Account.spec.js**

```javascript
require('should');
const proxyquire = require('proxyquire');

const sumTwoValuesStub = sinon.stub().returns(3000);

const AccountService = proxyquire('AccountService', {
    'Util': {
        sumTwoValues: sumTwoValuesStub
    }
});

describe('AccountService', () => {
    describe('#getAverage', () => {
        it('should return the average value', () => {
            const accountService = new AccountService();

            const result = accountService.getAverage(1000, 2000);
            result.should.equals(1500);
            sinon.assert.calledWith(sumTwoValuesStub, 1000, 2000);
        });
    });
});
```

Vemos que en vez de utilizar `require` como venimos utilizando hasta el momento se emplea el método `proxyquire` junto con la ruta del módulo a importar y un objeto. Este objeto estará compuesto por aquellas dependencias que queremos reemplazar y con qué funciones/valores. Entonces importamos nuestro módulo en cuestión que vamos a probar y lo aislamos de sus dependencias, reemplazandolas por dobles de prueba.

El objeto que reemplaza a las dependencias tendrá como nombre de atributo las rutas de las dependencias a reemplazar y como valor, objetos, métodos o clases que devolveran las dependencias cuando el módulo en cuestión las llame. Para el ejemplo, vemos que utilizamos como nombre de atributo el nombre de la dependencia tal cual esta escrita dentro de `AccountService`. Como valor devolvemos un objeto con un sólo atributo el cual consiste en la función `sumTwoValues` que ahora es un `stub`. A partir de ahora, cuando `getAverage` sea ejecutada y esta a su vez llame a `sumTwoValues`, se ejecutará el `stub` y no la función original.

## Dependencias según la ruta

Veamos el caso en el que el módulo **Util.js** se encuentra en otra carpeta, por ejemplo `/helpers/`:

>**Account.js (2)**

```javascript
const {sumTwoValues} = require('./helpers/Util');

class AccountService {
    getAverage (firstAccountBalance, lastAccountBalance) {
        return sumTwoValues(firstAccountBalance, lastAccountBalance) / 2;
    }
}

module.exports = AccountService;
```

>**Account.spec.js (2)**

```javascript
require('should');
const proxyquire = require('proxyquire');

const sumTwoValuesStub = sinon.stub().returns(3000);

const AccountService = proxyquire('AccountService', {
    './helpers/Util': {
        sumTwoValues: sumTwoValuesStub
    }
});

describe('AccountService', () => {
    describe('#getAverage', () => {
        it('should return the average value', () => {
            const accountService = new AccountService();

            const result = accountService.getAverage(1000, 2000);
            result.should.equals(1500);
            sinon.assert.calledWith(sumTwoValuesStub, 1000, 2000);
        });
    });
});
```

Sencillamente, al usar **Proxyquire**, en el nombre de atributo debemos coincidir el mismo con la ruta utilizada en el archivo **Util.js** relativo al archivo **Account.js**. En otras palabras, colocar exactamente la misma ruta con la que se importa la dependencia.

## Dependencias según el tipo de módulo

En el caso que no trabajemos con dependencias que sean objetos, deberemos adaptar nuestro trabajo para devolver el doble de prueba de manera adecuada. Suponiendo que nuestro método sea uno directamente exportado y no un método proveniente de un objeto:

>**/util/SumTwoValues.js**

```javascript
module.exports = (a, b) => a + b;
```

Entonces nuestro ejemplo ahora tendrá una ruta distinta dentro de la carpeta `/util/` con el nombre **SumTwoValues.js**. Debemos actualizar nuestro archivo `Account.js` para que utilice la dependencia correctamente:

>**Account.js (3)**

```javascript
const sumTwoValues = require('./util/SumTwoValues');

class AccountService {
    getAverage (firstAccountBalance, lastAccountBalance) {
        return sumTwoValues(firstAccountBalance, lastAccountBalance) / 2;
    }
}

module.exports = AccountService;
```

Ahora que nuestra dependencia no es un objeto sino una función debemos también indicarle a **Proxyquire** que la dependenciena que necesitamos reemplazar no consta de un objeto sino de una función:

>**Account.spec.js (3)**

```javascript
require('should');
const proxyquire = require('proxyquire');

const sumTwoValuesStub = sinon.stub().returns(3000);

const AccountService = proxyquire('AccountService', {
    './util/SumTwoValues': sumTwoValuesStub
});

describe('AccountService', () => {
    describe('#getAverage', () => {
        it('should return the average value', () => {
            const accountService = new AccountService();

            const result = accountService.getAverage(1000, 2000);
            result.should.equals(1500);
            sinon.assert.calledWith(sumTwoValuesStub, 1000, 2000);
        });
    });
});
```

## Ciclo de vida de la inyección de dependencias

La funcionalidad de **Proxyquire** esta hecha de tal manera que cuando el módulo es importado y sus dependencias reemplazadas, el mismo es un módulo nuevo que no reemplaza la funcionalidad original, de tal manera que otros módulos dependiendo de el no se verán afectados. Esto es más visible en pruebas de integración que en pruebas unitarias, aunque puede darse el caso que en una misma suite de pruebas unitarias se de el caso de necesitar más de una instancia de un mismo módulo.

Una vez que el módulo se desecha o termina el contexto de ejecución o la prueba unitaria y el módulo no es necesario, no modifica el código original por lo que puede volverse a importar el módulo original o ser importado con un nuevo juego de dependencias. Cabe advertir que de haber dobles de prueba asignados a reemplazar dependencias, estos continuarán manteniendo su historial a pesar de haberse destruido el módulo que los tenía asignados. Si no se tiene en consideración esto, al evaluar los dobles de prueba pueden darse resultados inesperados a la hora de ejecutar las aseveraciones:

>**Account.spec.js (4)**

```javascript
require('should');
const proxyquire = require('proxyquire');

const sumTwoValuesStub = sinon.stub().returns(3000);

describe('AccountService', () => {
    describe('#getAverage', () => {
        it('should return the average value', () => {
            const AccountService = proxyquire('AccountService', {
                './util/SumTwoValues': sumTwoValuesStub
            });
            const accountService = new AccountService();

            const result = accountService.getAverage(1000, 2000);
            result.should.equals(1500);
            sinon.assert.calledOnce(sumTwoValuesStub, 1000, 2000);
        });

        it('should return the average value again', () => {
            const AccountService = proxyquire('AccountService', {
                './util/SumTwoValues': sumTwoValuesStub
            });
            const accountService = new AccountService();

            const result = accountService.getAverage(1000, 2000);
            result.should.equals(1500);

            // This will fail because sumTwoValuesStub has been called twice
            sinon.assert.calledOnce(sumTwoValuesStub, 1000, 2000);
        });
    });
});
```

La segunda prueba unitaria fallará al ser ejecutada al intentar aseverar que el doble de prueba `sumTwoValuesStub` fue llamado una sola vez. La respuesta es sencilla: `sumTwoValuesStub` guarda en su historial una llamada al mismo en la prueba unitaria anterior. Aunque el primer `AccountService` no existe, el `stub` sigue existiendo y registra una llamada en su historial. En la segunda prueba unitaria, el segundo `AccountService` vuelve a utilizar el mismo `stub` que antes, con su historial intacto. Entonces a la hora de aseverar la cantidad de llamadas la misma falla porque al momento de aseverar se habrá registrado la segunda llamada a la función. Es por ello que es muy importante considerar el contexto y el propósito de los `stubs` en cada prueba unitaria a fin de no entrar en problemas.
