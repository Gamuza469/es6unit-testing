﻿# Creando pruebas unitarias a partir de código existente

En los casos que necesitamos probar código con un flujo que se ramifica debemos utilizar `stubs` para controlar el flujo de ejecución en una prueba determinada.

>**Nota**
>Para los siguientes ejemplos se utilzará un entorno de NodeJS, utilizando Mocha, Should.js y Sinon.js.

Veamos el siguiente código:

>**Util.js**

```javascript
const {includes, isNull} = require('lodash');

module.exports = {
    getPermissions: userRole => {
        let permissions = null;

        if (userRole === 'admin') {
            permissions = 'all';
        } else if (userRole === 'areaManager') {
            permissions = {editUsers: true, viewForms: true};
        } else if (includes(['supervisor', 'coordinator'], userRole)) {
            permissions = 'editUsers';
        } else if (isNull(userRole)) {
            permissions = 'login';
        } else {
            permissions = false;
        }

        return permissions;
    }
};
```

El método `getPermissions` nos retorna diferentes valores de permisos dependiendo del rol de usuario provisto. Además que estos valores no son del mismo tipo. Este método será utilizado en la siguiente clase:

>**PersonService.js**

```javascript
const {getPermissions} = require('Util');
const {isObject} = require('lodash');

class PersonService {
    canEditUser (userRole) {
        const permissions = getPermissions(userRole);

        if (permissions === 'all' || permissions === 'editUsers' || (isObject(permissions) && permissions.editUsers)) {
            return true;
        }

        if (!permissions) {
            throw new Exception(
                'User is either not logged in or its role has insufficient permissions.'
            );
        }

        return false;
    }
}

module.exports = PersonService;
```

Aquí vemos que nuestro método `canEditUser` es utilizado para obtener los permisos del usuario acreditado actual. Dependiendo de nuestros permisos, el método nos dirá si podemos editar usuarios o no. En el caso que el usuario no tenga un rol válido, se lanzará una excepción.

En nuestra prueba unitaria evaluaremos los diferentes caminos del método `canEditUsers`. Por un lado probaremos que el método funciona y por el otro que los resultados para los mismos parámetros ingresados sean siempre los mismos.

>**PersonService.spec.js (1)**

```javascript
require('should');
const sinon = require('sinon');
const PersonService = require('./PersonService');

describe('PersonService', () => {
    describe('#canEditUsers', () => {
        it('should tell if the current logged in user can edit users', () => {
            const personService = new PersonService();
            const result = personService.canEditUsers('admin');
            result.should.be.false();
        });
    });
});
```

En el ejemplo **(1)**, nuestra prueba cumple el propósito de evaluar `canEditUsers` pero tiene algunas deficiencias. Primero, sólo se prueba un caso de uso o un camino del código. Al desarrollar una prueba unitaria debemos probar todos los caminos posibles. Segundo, no se aisló completamente la pieza de código por el hecho de que `canEditUsers` utiliza `getPermissions` que es una función ajena al código. Pasemos a corregir cada problema:

>**PersonService.spec.js (2)**

```javascript
require('should');
const sinon = require('sinon');
const PersonService = require('./PersonService');

describe('PersonService', () => {
    describe('#canEditUsers', () => {
        it('should tell if the current logged in user can edit users', () => {
            const personService = new PersonService();
            const canAdminEdit = personService.canEditUsers('admin');
            canAdminEdit.should.be.true();

            const canAreaManagerEdit = personService.canEditUsers('areaManager');
            canAreaManagerEdit.should.be.true();

            const canSupervisorEdit = personService.canEditUsers('supervisor');
            canSupervisorEdit.should.be.true();

            const canCoordinatorEdit = personService.canEditUsers('coordinator');
            canCoordinatorEdit.should.be.true();

            const canGuestEdit = personService.canEditUsers();
            canGuestEdit.should.be.false();

            const canPollsterEdit = () => personService.canEditUsers('pollster');
            (canPollsterEdit).should.throw();
        });
    });
});
```

Con esta prueba conseguimos cubrir el 100% del código de `canEditUsers`. Sin embargo seguimos usando  `getPermissions` que es un método externo al método que estamos probando en cuestión. La aproximación no está mal si estuviesemos probando los permisos obtenidos en base a los roles provistos pero aquí estamos verificando la habilidad para editar usuarios en base a los permisos obtenidos. Otro asunto a arreglar es la prueba en sí, donde se agrupan todos los casos en una sola prueba. Esto se podría separar en pruebas dependiendo del rol evaluado.

Esta aproximación demuestra que existe funcionalidad que no conocemos y por ende debemos probarla también, aunque esta funcionalidad no pertenezca al contexto de la pieza de código que estamos evaluando. Es ahí que tenemos que darnos cuenta que, de ocurrir esta situación, estamos ante la presencia de código externo que no podemos controlar porque **"no sabemos cómo funciona"** cuando lo usamos en nuestro código. Cuando se da este caso, debemos aislar aquel componente foráneo y controlarlo para que nuestra prueba unitaria este completamente libre de comportamientos no controlados. Para ello utilizaremos `stubs`.

Debe quedar en claro que debemos realizar pruebas unitarias tanto para para el método `getPermissions` como para `canEditUsers`, pero por ahora nos concentraremos en obtener nuestras pruebas para `canEditUsers`.

>**PersonService.spec.js (3)**

```javascript
require('should');
const sinon = require('sinon');
const Util = require('./Util');
const PersonService = require('./PersonService');

const util = new Util();
const getPermissions = sinon.stub(util, 'getPermissions');
const personService = new PersonService();

describe('PersonService', () => {
    describe('#canEditUsers', () => {
        context('when the user is an admin', () => {
            it('should allow editing users', () => {
                getPermissions.returns('all');
                const canAdminEdit = personService.canEditUsers('admin');
                canAdminEdit.should.be.true();

                sinon.assert.called(getPermissions);
                getPermissions.reset();
            });
        });

        context('when the user is an area manager', () => {
            it('should allow editing users', () => {
                getPermissions.returns({editUsers: true});
                const canAreaManagerEdit = personService.canEditUsers('areaManager');
                canAreaManagerEdit.should.be.true();

                sinon.assert.called(getPermissions);
                getPermissions.reset();
            });
        });

        context('when the user is a supervisor', () => {
            it('should allow editing users', () => {
                getPermissions.returns('editUsers');
                const canSupervisorEdit = personService.canEditUsers('supervisor');
                canSupervisorEdit.should.be.true();

                sinon.assert.called(getPermissions);
                getPermissions.reset();
            });
        });

        context('when the user is a coordinator', () => {
            it('should allow editing users', () => {
                getPermissions.returns('editUsers');
                const canCoordinatorEdit = personService.canEditUsers('coordinator');
                canCoordinatorEdit.should.be.true();

                sinon.assert.called(getPermissions);
                getPermissions.reset();
            });
        });

        context('when the user is a guest', () => {
            it('should forbid it from editing users', () => {
                getPermissions.returns('login');

                const guest = null;
                const canGuestEdit = personService.canEditUsers(guest);
                canGuestEdit.should.be.false();

                sinon.assert.called(getPermissions);
                getPermissions.reset();
            });
        });

        context('when the user is a pollster or any other role', () => {
            it('should throw an exception', () => {
                getPermissions.returns(false);
                (() => personService.canEditUsers('pollster')).should.throw();

                sinon.assert.called(getPermissions);
                getPermissions.reset();
            });
        });
    });
});
```

Nuestra suite de pruebas unitarias creció pero con la ventaja que ahora tenemos aislado el comportamiento de `canEditUsers` de `getPermissions`. Eso se debe a que gracias al `stub` que creamos de la función `getPermissions`, le decimos exactamente que es lo que queremos que devuelva. De esta manera podemos controlar el flujo del código y cubrir todas las ramificaciones posibles dentro de `canEditUsers`.

Se puede observar cómo separamos nuestra prueba unitaria en pruebas más específicas. Incluso los mensajes se han actualizado para mostrar la especificidad de cada prueba. Es aquí que vemos el resultado de una buena estructuración de nuestras pruebas.

Ahora bien, podemos mejorar un poco más nuestras pruebas, utilizando `hooks` para automatizar las tareas de resetear nuestro `stub`.

>**PersonService.spec.js (4)**

```javascript
require('should');
const sinon = require('sinon');
const Util = require('./Util');
const PersonService = require('./PersonService');

const util = new Util();
const getPermissions = sinon.stub(util, 'getPermissions');
const personService = new PersonService();

describe('PersonService', () => {
    describe('#canEditUsers', () => {
        afterEach(() => {
            sinon.assert.called(getPermissions);
            getPermissions.reset();
        });

        context('when the user is an admin', () => {
            it('should allow editing users', () => {
                getPermissions.returns('all');
                const canAdminEdit = personService.canEditUsers('admin');
                canAdminEdit.should.be.true();
            });
        });

        context('when the user is an area manager', () => {
            it('should allow editing users', () => {
                getPermissions.returns({editUsers: true});
                const canAreaManagerEdit = personService.canEditUsers('areaManager');
                canAreaManagerEdit.should.be.true();
            });
        });

        context('when the user is a supervisor', () => {
            it('should allow editing users', () => {
                getPermissions.returns('editUsers');
                const canSupervisorEdit = personService.canEditUsers('supervisor');
                canSupervisorEdit.should.be.true();
            });
        });

        context('when the user is a coordinator', () => {
            it('should allow editing users', () => {
                getPermissions.returns('editUsers');
                const canCoordinatorEdit = personService.canEditUsers('coordinator');
                canCoordinatorEdit.should.be.true();
            });
        });

        context('when the user is a guest', () => {
            it('should forbid it from editing users', () => {
                getPermissions.returns('login');

                const guest = null;
                const canGuestEdit = personService.canEditUsers(guest);
                canGuestEdit.should.be.false();
            });
        });

        context('when the user is a pollster or any other role', () => {
            it('should throw an exception', () => {
                getPermissions.returns(false);
                (() => personService.canEditUsers('pollster')).should.throw();
            });
        });
    });
});
```

Si bien gracias al `stub`, el valor del parámetro `userRole` pasa a ser irrelevante para `canEditUsers` dado que el método en sí no utiliza el argumento de ninguna forma, es una buena práctica generar nuestras pruebas unitarias lo más posibles a la realidad, con datos relevantes. Esto difiere de utilizar `stubs` porque, en este caso, nosotros sí estamos probando cómo funciona `canEditUsers` y además `userRole` pertenece al contexto de la pieza de código que debemos probar. Además, nos puede ayudar a ahorrarnos dolores de cabeza si en un momento del ciclo de vida de la aplicación, `canEditUsers` obtiene funcionalidad extra propia cuya ejecución de un resultado que sí dependa del valor de `userRole`.

Entonces, ya deberíamos dar por terminado este asunto salvo un detalle. En cada prueba guardamos el resultado del método `canEditUsers` en una variable. Esto puede ser necesario para mejorar la legibilidad pero en nuestro caso es posible prescindir de la creación de dicha variable y directamente aseverar el resultado:

>**PersonService.spec.js (5)**

```javascript
require('should');
const sinon = require('sinon');
const Util = require('./Util');
const PersonService = require('./PersonService');
const util = new Util();
const getPermissions = sinon.stub(util, 'getPermissions');
const personService = new PersonService();
const canEditUsers = personService.canEditUsers;

describe('PersonService', () => {
    describe('#canEditUsers', () => {
        afterEach(() => {
            sinon.assert.called(getPermissions);
            getPermissions.reset();
        });

        context('when the user is an admin', () => {
            it('should allow editing users', () => {
                getPermissions.returns('all');
                canEditUsers('admin').should.be.true();
            });
        });

        context('when the user is an area manager', () => {
            it('should allow editing users', () => {
                getPermissions.returns({editUsers: true});
                canEditUsers('areaManager').should.be.true();
            });
        });

        context('when the user is a supervisor', () => {
            it('should allow editing users', () => {
                getPermissions.returns('editUsers');
                canEditUsers('supervisor').should.be.true();
            });
        });

        context('when the user is a coordinator', () => {
            it('should allow editing users', () => {
                getPermissions.returns('editUsers');
                canEditUsers('coordinator').should.be.true();
            });
        });

        context('when the user is a guest', () => {
            it('should forbid it from editing users', () => {
                getPermissions.returns('login');

                const guest = null;
                canEditUsers(guest).should.be.false();
            });
        });

        context('when the user is a pollster or any other role', () => {
            it('should throw an exception', () => {
                getPermissions.returns(false);
                (() => canEditUsers('pollster')).should.throw();
                (() => canEditUsers('assignator')).should.throw();
            });
        });
    });
});
```

De esta manera nuestro código queda más limpio. Nuestro método tiene un nombre acertado para la función que realiza por lo que nuestro código de prueba se ve natural sin necesidad de variables intermedias.

Se puede apreciar en la última prueba que hay otro rol más, el cual es `assignator`. Si en nuestro proyecto existiese tal rol, es nuestro deber corroborar el comportamiento de nuestro código al utilizar dicho rol. Si se quiere comprobar una funcionalidad para valores *no contemplados o no válidos* para el código, los valores utilizados deben ser acotados en cantidad. Suponiendo que `assignator` realmente no fuese parte de los roles que se manejen en el sistema, podemos suponerlo como un rol ficticio pero factible de existir que pueda reflejar de manera natural el comportamiento del código ante este rol ficticio.

