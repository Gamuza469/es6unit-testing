﻿# Utilización de los hooks

Los `hooks` son funciones que complementan las tareas de pruebas unitarias. Con ellos podemos especificar qué tareas realizar antes o después de la ejecución y finalización de una prueba unitaria.

>**Nota**

Para nuestra descripción se describirá la funcionalidad de los hooks del framework Mocha.

## Los diferentes hooks

En el ambiente de las pruebas unitarias se pueden distinguir cuatro `hooks` distintos: `before`, `beforeEach`, `after`, `afterEach`. Como sus nombres en inglés lo determinan, cada uno se encargará de ejecutar las tareas necesarias en el momento correspondiente.

Nota: si se utiliza **Jest**, `before` y `after` se usan como `beforeAll` y `afterAll`.

## Definición de un hook

Un `hook` se puede definir dentro de un bloque `describe` o un bloque `context` y no pueden ser definidos dentro de un bloque `it`. Según dentro de qué bloque se definan, estos tendrán un orden de ejecución determinado. Se pueden definir varios `hooks` de un mismo tipo inclusive dentro del mismo bloque.

La definición de un `hook` del tipo `after` o `afterEach` no necesita darse explícitamente luego de las pruebas. Pueden definirse inmediatamente luego de un `hook` `before` o `beforeEach`.

## Orden de ejecución de los hooks

Según la documentación oficial de **Mocha**, los `hooks` del tipo `before` se ejecutan primero antes que cualquier otro. Luego, los `hooks` del tipo `beforeEach`. Después de ejecutar ambos `hooks` se procede a ejecutar la prueba. Una vez finalizada la misma, se ejecutan todos los `hooks` del tipo `afterEach` seguidos por los `hooks` del tipo `after`.

Si hay más de una definición de un mismo tipo de `hook`, estos se ejecutan en el mismo orden en el que fueron declarados, siempre respetando el bloque o contexto donde fueron definidos.

## before() y after()

Estos `hooks` sólo se ejecutan una vez dentro del bloque en el cual se definieron. La ejecución del `before` se da antes de la primer prueba unitaria del bloque en el cuál fue definido. La ejecución del `after` se da luego de ejecutar la última prueba del bloque.

## beforeEach() y afterEach()

Estos `hooks` se ejecutan una vez por cada prueba unitaria dentro del bloque en el cual se definieron. La ejecución del `beforeEach` se da antes de iniciar cada prueba unitaria del bloque en el cuál fue definido. La ejecución del `afterEach` se da luego de ejecutar cada prueba del bloque.

## Utilidad de los hooks

Una de las tareas más comunes para las cuales se usan `hooks` son las de inicializar o restaurar los `test doubles`. Delegando dichas tareas a los `hooks` nos permite liberar a las pruebas unitarias de la responsabilidad de configurar los `test doubles`:

>**util.spec.js**

```javascript
describe('UtilService', () => {
    let sum = null;

    beforeEach('clear spy', () => {
        sum = sinon.spy();
    });

    it('sums two values', () => {
        sum(2, 4);
        sinon.assert.calledOnce(sum);
    });

    it('sums four values', () => {
        sum(2, 4, 10, -1);
        sinon.assert.calledOnce(sum);
        sinon.assert.calledWith(sum, 2, 4, 10, -1);
    });
});
```

Otros posibles ejemplos de uso consistirían en crear instancias de una clase o limpiar el historial de un `spy` o un `stub`.
