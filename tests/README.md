# Lista de contenidos

1. [Creando pruebas unitarias a partir de código existente](./pruebas-a-partir-de-codigo-existente.md)
2. [Utilización de los hooks](./utilizacion-hooks.md)
3. [Reemplazando dependencias para pruebas unitarias](./reemplazando-dependencias.md)
4. [Cómo testear métodos privados o no exportados](./probar-metodos-privados.md)
