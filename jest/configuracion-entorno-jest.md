﻿# Configuración de un entorno de pruebas en Jest

## Dependencias (package.json)

Necesitaremos instalar los siquientes paquetes (versiones al día de la fecha):

```json
"devDependencies": {
    "@babel/core": "^7.2.2",
    "@babel/plugin-transform-modules-commonjs": "^7.2.0",
    "@babel/preset-react": "^7.0.0",
    "babel-core": "^7.0.0-bridge.0",
    "babel-jest": "^23.6.0",
    "chai": "^4.2.0",
    "chai-enzyme": "^1.0.0-beta.1",
    "enzyme": "^3.8.0",
    "enzyme-adapter-react-16": "^1.7.1",
    "enzyme-to-json": "^3.3.5",
    "jest": "^23.6.0",
    "jest-cli": "^23.6.0",
    "jest-plugin-context": "^2.9.0"
},
```

### Paquetes principales

- `@babel/core` - Paquete principal de Babel. Necesario para que Jest transpile nuestro código.
- `chai` - Biblioteca que se encargará de manejar los `assertions`.
- `enzyme` - Framework para explorar componentes React.
- `jest` - Framework de pruebas principal.
- `babel-jest` - Permite a Jest utilizar Babel.

### Paquetes auxiliares

- `@babel/preset-react` - Permite transpilar componentes React.
- `@babel/plugin-transform-modules-commonjs`- Permite transpilar módulos ES6 a CommonJS.
- `babel-core` - A diferencia de su par `@babel/core`, este plugin permite transpilar paquetes que tengan instrucciones de transpilación hechas para Babel v6, en un entorno que utiliza Babel v7 en adelante, como el nuestro. Caso contrario, no es necesario este paquete.
- `chai-enzyme` - Agrega `assertions` a la biblioteca Chai para realizar evaluaciones sobre elementos de Enzyme.
- `enzyme-adapter-react-16` - Paquete que le permite a Enzyme trabajar con el entorno de React necesario. La versión 16 permite trabajar con React v16 en adelante.
- `enzyme-to-json` - Permite crear `snapshots` o instantáneas de un componente React y guardarlo en formato JSON.
- `jest-cli` - Nos permite manejar Jest mediante línea de comandos.
- `jest-plugin-context` - Plugin especial que nos permite utilizar bloques `context` en nuestras pruebas.

## Configuración Babel v7

Se presenta la configuración base para utilizar Babel al correr nuestras pruebas en Jest.

Primero y principal, se debe tener en cuenta que Jest no toma de manera correcta los archivos `.babelrc`, por ende siempre se debe usar `babel.config.js` para configurar Babel al ser utilizado por Jest. En caso que sea necesario, se pueden utilizar archivos `.babelrc`, además del archivo `babel.config.js`, según sea necesario cambiar la configuración de Babel.

Al ejecutarse, Jest modifica la variable `env` del proceso de NodeJs a `test` automáticamente. Sabiendo esto, debemos colocar toda nuestra configuración específica de Babel cuando corremos Jest, en el entorno de `test`.

Tener en cuenta, en caso de necesitarlo, de usar archivos `.babelrc.js` en vez de los comunes `.babelrc` escritos en formato JSON.

```javascript
module.exports = {
    presets: [
        '@babel/env',
        '@babel/react'
    ],
    env: {
        test: {
            plugins: [
                '@babel/transform-modules-commonjs'
            ]
        },
    }
};
```

## Configuración Jest

Además de configurar parámetros de traspilación del código o las pruebas unitarias también se pueden configurar el análisis de cobertura, archivos de configuración del entorno de prueba y archivos a ignorar en las pruebas, entre otras cosas.

>**jest.config.js**

```javascript
module.exports = {
    coverageReporters: ['text', 'html', 'lcov'],
    snapshotSerializers: ['enzyme-to-json/serializer'],
    transformIgnorePatterns: [
        'node_modules/(?!(react|react-native)/)'
    ],
    testPathIgnorePatterns: [
        '/node_modules/',
        '/gulp/'
    ],
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/tests/fileMock.js',
        '\\.(css|less)$': '<rootDir>/tests/styleMock.js'
    },
    setupTestFrameworkScriptFile: '<rootDir>/tests/setup.js',
    setupFiles: [
        'chai/register-should',
        'jest-plugin-context/setup'
    ]
};
```

Ahora describiremos algunos de los atributos del objeto de configuración:

### transformIgnorePatterns

En Jest, este atributo nos permite establecer, mediante expresiones regulares, aquellos archivos o paquetes que Babel no debe traspilar. En este caso, la única expresión regular que aparece en nuestro `array` le dice a Jest que evite traspilar todos aquellos paquetes **npm** a excepción de los paquetes **react** y **react-native**. Parafraseando, **Jest** indicará a **Babel** que transpile todos nuestros archivos, incluídos los archivos de pruebas unitarias y solamente los paquetes **react** y **react-native**, dejando los demás paquetes **npm** sin traspilar.

Esta aproximación es la más práctica dado que en la práctica no queremos que se traspilen paquetes **npm** porque por lo común ya se encuentran traspilados a ES5 salvo algunas excepciones. Por ejemplo, algunos paquetes privados o paquetes que se encuentran en desarrollo pueden no estar traspilados y por ende no pueden ser interpretados por **Jest**. Es por ello que agregamos estas excepciones con una expresión regular con `negative-lookup` para que solo agregue a la lista aquellos paquetes que especificamos entre paréntesis.

### moduleNameMapper

Podemos configurar mediante este atributo, aquellos archivos que queremos transformar. Cada una de las líneas responde a expresiones regulares y de acertar a alguna se ejecuta el archivo transformador. En nuestro caso tenemos dos líneas cuyas expresiones regulares responden a grupos de extensiones de archivo. Estos grupos pertenecen a imágenes o a plantillas de estilo.

Estos archivos son hacen falta probarlos y **Jest** no los podrá interpretar asi que debemos ignorarlos. Para ignorarlos debemos transformarlos en `stubs`. En el caso de no crear estos `stubs`, **Jest** fallará.

Según el tipo de archivo tendremos dos archivos transformadores, los cuales son archivos muy sencillos en nuestro caso. Cada uno de esos archivos procesarán de una manera en particular sus objetivos, devolviendo un `stub` por cada archivo procesado:

>**/tests/fileMock.js**

```javascript
module.exports = 'test-file-stub';
```

>**/tests/styleMock.js**

```javascript
module.exports = {};
```

### setupTestFrameworkScriptFile

Podemos definir un archivo donde se inicialice código necesario para poder realizar las pruebas unitarias o importar los módulos necesarios para que estos estén disponible antes de cada prueba unitaria. La ventaja más evidente es la de evitar volver a importar todos los módulos necesarios en cada suite de pruebas unitarias. Cabe destacar que `setupTestFrameworkScriptFile` se ejecutará luego de los archivos declarados en `setupFiles`.

>**/tests/setup.js**

```javascript

import enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());
enzyme.configure({adapter: new Adapter()});
```

### setupFiles

Podemos definir varios archivos o módulos que inicialicen código necesario para configurar el entorno de pruebas. Este código se ejecuta antes de ejecutar cada archivo de pruebas. Además, los `setupFiles` se ejecutan *antes* que `setupTestFrameworkScriptFile`.

En nuestro caso, inicializamos la biblioteca `chai-should` y `jest-plugin-context` para poder utilizar `context` en **Jest**.
