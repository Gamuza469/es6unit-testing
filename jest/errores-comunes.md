﻿# Jest troubleshooting

Lista de problemas más comunes y sus soluciones:

## Path is not a string

En el caso que al iniciar Jest, generalmente en un nuevo entorno de pruebas, nos salte un error acerca del `path` siendo `undefined`. Esto se debe a que falta instalar el paquete `jest-cli`:

```sh
npm install --save-dev jest-cli
```

Al correr los tests, el error desaparece y se ejecutan los mismos.

## Undefined token 'import/export'

Jest no puede entender `ES6` por lo que depende de `Babel` para que este transpile el código a `ES5`, es decir, código que los navegadores puedan entender. El ejemplo más común se da cuando estamos ante un proyecto que utiliza `imports` o `exports` en vez de `require.js` para llamar a otros módulos.

Luego de corroborar que la configuración de `Babel` en nuestro archivo `.babelrc` o `babel.config.js` este correcta, debemos instalar el paquete `babel-jest` de manera explícita. Si bien el sitio oficial de Jest indica que `babel-jest` se instala junto con Jest, la realidad es que lo referencia automáticamente pero se debe instalar por separado:

```sh
npm install --save-dev babel-jest
```

Con este paquete y una buena configuración de `Babel` nuestro proyecto y nuestras pruebas deberían ser transpiladas correctamente.

## Requires Babel "^7.0.0-0", but was loaded with "6.26.3"

Jest utiliza Babel v7. Al toparse con módulos configurados para transpilar usando Babel v6 nos muestra este error. Para solucionarlo, hay que instalar el paquete `babel-core` en su versión *bridge*:

```sh
npm install --save-dev babel-core@^7.0.0-bridge.0
```

Cabe destacar que la última versión de Babel v6 (`babel-core@6.26.3`) no sirve a la hora de usar Jest.

Una vez instalado el paquete el problema debería resolverse.
